import { SessionStorage } from "quasar";
import { api } from "boot/axios";
const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "orders",
        name: "home",
        component: () => import("src/pages/orders/generateOrder.vue"),
        // component: () => import("pages/Index.vue"),
        beforeEnter: (to, from, next) => {
          // ...
          let token = SessionStorage.getItem("jwt");
          if (token === null) {
            api
              .get("returnSession", {
                headers: {
                  accept: "application/json",
                },
              })
              .then((response) => {
                SessionStorage.set("jwt", response.data.message);
                token = SessionStorage.getItem("jwt");
                //next({ name: "home" });
                next();
              })
              .catch((error) => {
                next({ name: "login" });
              });
          } else {
            next();
          }
        },
      },
      /*  {
        path: "login",
        name: "login",
        component: () => import("src/pages/login/login.vue"),
      }, */
      {
        path: "mediosPagos",
        name: "mediosPagos",
        component: () => import("src/pages/payments/listPayments.vue"),
        beforeEnter: async (to, from, next) => {
          // ...
          let token = SessionStorage.getItem("jwt");
          if (token === null) {
            await api
              .get("returnSession", {
                headers: {
                  accept: "application/json",
                },
              })
              .then((response) => {
                SessionStorage.set("jwt", response.data.message);
                token = SessionStorage.getItem("jwt");

                //next({ name: "home" });
                next();
              })
              .catch((error) => {
                next({ name: "login" });
              });
          } else {
            next();
          }
        },
      },
      {
        path: "nuevoPedido",
        name: "nuevoPedido",
        component: () => import("src/pages/orders/AddOrders.vue"),
        beforeEnter: async (to, from, next) => {
          // ...
          let token = SessionStorage.getItem("jwt");
          if (token === null) {
            await api
              .get("returnSession", {
                headers: {
                  accept: "application/json",
                },
              })
              .then((response) => {
                SessionStorage.set("jwt", response.data.message);
                token = SessionStorage.getItem("jwt");

                //next({ name: "home" });
                next();
              })
              .catch((error) => {
                next({ name: "login" });
              });
          } else {
            next();
          }
        },
      },
      {
        path: "productos",
        name: "productos",
        component: () => import("src/pages/products/listProducts.vue"),
        beforeEnter: async (to, from, next) => {
          // ...
          let token = SessionStorage.getItem("jwt");
          if (token === null) {
            await api
              .get("returnSession", {
                headers: {
                  accept: "application/json",
                },
              })
              .then((response) => {
                SessionStorage.set("jwt", response.data.message);
                token = SessionStorage.getItem("jwt");

                //next({ name: "home" });
                next();
              })
              .catch((error) => {
                next({ name: "login" });
              });
          } else {
            next();
          }
        },
      },
      {
        path: "users",
        name: "users",
        component: () => import("pages/users/listUsers.vue"),
        beforeEnter: async (to, from, next) => {
          // ...
          let token = SessionStorage.getItem("jwt");
          if (token === null) {
            await api
              .get("returnSession", {
                headers: {
                  accept: "application/json",
                },
              })
              .then((response) => {
                SessionStorage.set("jwt", response.data.message);
                token = SessionStorage.getItem("jwt");

                //next({ name: "home" });
                next();
              })
              .catch((error) => {
                next({ name: "login" });
              });
          } else {
            next();
          }
        },
      },
      /*     {
        path: "/orders",
        name: "orders",
        component: () => import("src/pages/orders/generateOrder.vue"),
      }, */
      {
        path: "/orderStates",
        name: "orderStates",
        component: () => import("src/pages/orders/orderEstados.vue"),
        beforeEnter: async (to, from, next) => {
          // ...
          let token = SessionStorage.getItem("jwt");
          if (token === null) {
            await api
              .get("returnSession", {
                headers: {
                  accept: "application/json",
                },
              })
              .then((response) => {
                SessionStorage.set("jwt", response.data.message);
                token = SessionStorage.getItem("jwt");

                //next({ name: "home" });
                next();
              })
              .catch((error) => {
                next({ name: "login" });
              });
          } else {
            next();
          }
        },
      },
      /* {
        path: "/orders",
        name: "orders",
        component: () => import("pages/Orders"),
      }, */
      {
        path: "/orderGenerate",
        name: "orderGenerate",
        component: () => import("pages/orders/orderResponsePaypal.vue"),
        beforeEnter: async (to, from, next) => {
          // ...
          let token = SessionStorage.getItem("jwt");
          if (token === null) {
            await api
              .get("returnSession", {
                headers: {
                  accept: "application/json",
                },
              })
              .then((response) => {
                SessionStorage.set("jwt", response.data.message);
                token = SessionStorage.getItem("jwt");

                //next({ name: "home" });
                next();
              })
              .catch((error) => {
                next({ name: "login" });
              });
          } else {
            next();
          }
        },
      },
      {
        path: "/payments",
        name: "payments",
        component: () => import("pages/payments/payments.vue"),
        beforeEnter: async (to, from, next) => {
          // ...
          let token = SessionStorage.getItem("jwt");
          if (token === null) {
            await api
              .get("returnSession", {
                headers: {
                  accept: "application/json",
                },
              })
              .then((response) => {
                SessionStorage.set("jwt", response.data.message);
                token = SessionStorage.getItem("jwt");

                //next({ name: "home" });
                next();
              })
              .catch((error) => {
                next({ name: "login" });
              });
          } else {
            next();
          }
        },
      },
      {
        path: "/payments/create",
        name: "paymentscreate",
        component: () => import("pages/payments/create.vue"),
        beforeEnter: async (to, from, next) => {
          // ...
          let token = SessionStorage.getItem("jwt");
          if (token === null) {
            await api
              .get("returnSession", {
                headers: {
                  accept: "application/json",
                },
              })
              .then((response) => {
                SessionStorage.set("jwt", response.data.message);
                token = SessionStorage.getItem("jwt");

                //next({ name: "home" });
                next();
              })
              .catch((error) => {
                next({ name: "login" });
              });
          } else {
            next();
          }
        },
      },
    ],
  },
  {
    path: "",
    name: "login",
    component: () => import("pages/login/login.vue"),
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/Error404.vue"),
  },
];

export default routes;
