# MyAPP_Resto

## Requerimientos para el funcionamiento

Tener instalado docker y docker-compose

## Introducción

Presentando el Sprint Nro 4 - Creación de api y un front para pedidos de un restaurante

## Instrucciones de instalación

### Descarga el repositorio:

    git clone https://gitlab.com/gustavo.javier.sobol/myapp_resto

### Accede a la carpeta:

    cd myapp_resto/

## Configuraciones archivos env necesarios

### Copiar el archivo env-example-docker y renombrarlo como .env

**MYSQL_HOST=** host de la base de datos mysql ej: localhost

**MYSQL_PORT=** puerto donde corre el motor de base de datos ej: 3306

**MYSQL_PASS=** contraseña de la base de datos ej: root

**PORT_BACK=** puerto en el que se va a ejecutar la aplicación backend ej: 4000

**PORT_FRONT=** puerto en el que se va a ejecutar la aplicación frontend ej: 8181

**REDIS_HOST=** host donde se encuentra el servidor redis ej: redis

**REDIS_PORT=** puerto donde se encuentra el servidor redis ej: 6379

**PORT_NGINX=** puerto donde se encuentra el servidor nginx ej: 80

### Accder a la carpeta back (cd back) y Copiar el archivo env-example-back y renombrarlo como .env

**MYSQL_HOST=** host de la base de datos mysql ej: localhost **Debe coincidir con el archivo .env realizado en la carpeta raiz**

**MYSQL_PORT=** puerto donde corre el motor de base de datos ej: 3306 **Debe coincidir con el archivo .env realizado en la carpeta raiz**

**MYSQL_USER=** usuario para la conexion de la base de datos ej: root

**MYSQL_PASS=** contraseña de la base de datos ej: root **Debe coincidir con el archivo .env realizado en la carpeta raiz**

**MYSQL_DB_NAME=** nombre de la base de datos anteriormente creada ej: my_app_rest

**JWT_SECRET=** secreto para JsonWetTOKEN ej: 1234

**HOST=** ip internar del contenedor donde se esta ejecutando... en este caso esta configurado con la ip fija 100.100.100.12 , en el caso de tener un dominio aca debe ir el dominio

**PORT=** puerto en el que se va a ejecutar la aplicación backend ej: 4000 **Debe coincidir con el archivo .env realizado en la carpeta raiz. Variable llamada PORT_BACK**

**REDIS_HOST=** host donde se encuentra el servidor redis ej: redis **Debe coincidir con el archivo .env realizado en la carpeta raiz**
**REDIS_PORT=** puerto donde se encuentra el servidor redis ej: 6379 **Debe coincidir con el archivo .env realizado en la carpeta raiz**

**HOST_FRONT=** ip internar del contenedor donde se esta ejecutando... en este caso esta configurado con la ip fija 100.100.100.11 , en el caso de tener un dominio aca debe ir el dominio

**GOOGLE_CLIENT_ID=** Id del cliente de google obtenido desde para google auth
**GOOGLE_CLIENT_SECRET=** Secret del cliente de google obtenido desde para google auth

**LINKEDIN_CLIENT_ID=** Id del cliente obtenido desde para linkedin auth
**LINKEDIN_CLIENT_SECRET=** Secret obtenido desde para linkedin auth

**PAYPAL_CLIENT_ID=** Id obtenido desde para paypal
**PAYPAL_CLIENT_SECRET=** Secreto obtenido desde para paypal

**MERCADO_ACCESS_TOKEN=** id api obtenido desde mercado pago

### Accder a la carpeta front (cd ../front) y Copiar el archivo env-example-front y renombrarlo como .env

**PUERTO=** puerto en el que se va a ejecutar la aplicación backend ej: 4000 **Debe coincidir con el archivo .env realizado en la carpeta raiz. Variable llamada PORT_FRONT**

**API=** ip del servidor donde se esta ejecutando la api del backend... , en el caso de tener un dominio aca debe ir el dominio

## Configuracion de nginx

En el docker compose se encuentra corriendo un contenedor con el servicio de nginx donde se puede configurar su archivo default desde el exterior que se encuentra en la ruta **datos -> conf-nginx -> default.conf** Dentro de ese archivo se encuentra configurado de manera predeterminada para el desarrollo si desea pasarlo a un productivo debe comenta la linea desarrollo y descomentar la linea para produccion como se muestra en la imagen

<br>

<img  src="/imagenes/config_nginx.jpg" alt="Configuracion nginx" />

<br>

### Opcional pasar front a productivo

Se creo un pequeño front con quasar para la demo de la aplicacion
Para compilar el desarrollo del front a productivo se creo un script en bash que facilita los pasos de compilacion
**Antes de ejecutar el script si cambia de servidor se recomienda configurar el archivo .env de la carpeta front con los parametros del servidor de destino**

Para la compilacion ejecutar el comando

> sh a_produccion.sh

### Iniciando los servicios

1. Una vez realizado los pasos anteriores crear dentro de la carpeta datos mysql-bases-de-datos redis ejecutando los comandos

> cd datos

> mkdir mysql-bases-de-datos redis

> cd ..

2. Inicializar el servicio de mysql en docker

> docker-compose up -d mysql

Una vez que el servicio esta corriendo crear la base de datos con los parametros que se asigno en el archivo .env del back
usuario contraseña y nombre de base de datos

en mi caso las credenciales son usuario: **root** contraseña: **root** y la base de datos: **my_app_rest**

Debe reemplazar esos parametros en el siguiente comando en el caso que sean distintos

> docker exec -it mysql mysql -uroot -proot -e "create database my_app_rest;"

**Si el comando anterior emite un error esperar unos segundo que este corriendo el servicio correctamente y volver a ejecutar el comando**

Puede verificar la creacion la base de datos con el comando

> docker exec -it mysql mysql -uroot -proot -e "show databases;"

**si a aparece su base de datos continuar con el puto 3 sino repetir punto 2**

3. Instalo dependencias en el back
   Instalar todas las dependencias del package.json

> docker-compose run -it back npm i

Genera toda las tablas y datos necesarios en la base de datos

> docker-compose run back npm run inicio

4. En el ultimo desplegar todos los servicios

En este ultimo paso y para modo desarrollo del front debe ejecutar los siguientes comandos

Instala todas las dependencias necesarias para ejecutar el modo desarrollo en quasar

> docker-compose run -it front npm i

Despliega todos los servicios del docker compose **Recordar el que nginx este configurado para desarrollo**

> docker-compose up -d

en el caso que lo quiera desplegar como produccion debe ejecutar el siguiente comando:

Despliega todos los servicios necesarios sin el desarrollo del front

Instalo dependencias en el back: docker-compose up -d back

**_TODA LA CONFIGURACION PARA EL DESPLIEGUE FUE TESTEADO CON UBUNTU 18 Y UBUNTU 22_**

### Acceder a los endpoints a traves de swagger

1. Para acceder a los endpoints debe dirigirse a su navegador y colocar la url http://localhost/api/api-docs
   Y para acceder al front debe acceder a http://localhost

2. Para poder acceder a todos los endpoints debe loguearse en el sistema.
   En el sector login en el endpoint login debe ingresar usuario y contraseña. A continuación se daran los datos de acceso.

   **Usuario Administrador**

   usuario: admin

   contraseña: 123456

   **Usuario Pedidor**

   usuario: user

   contraseña: 123456

3. Una vez logueado el sistema emite un token de acceso para cada endpoint en el cual se puede cargar una sola vez en el botón **Authorize** o en cada endpoint en la imagen con el candado. Haciendo click en uno de ellos muestra una pantalla para que pueda pegar el token antes generado. Después cada vez que accede a un endpoint que posee el candado hay una verificación si el token que esta actualmente es valido o no ... en el caso que no sea valido el sistema emitira un mensaje indicando el error y debera loguearse nuevamente.

### Para poder acceder a la pagina de mi app resto

Para ingresar a swagger http://localhost/api/api-docs/

Para acceder al front http://localhost/

### [**Ver capturas del sistema** ](https://gitlab.com/gustavo.javier.sobol/myapp_resto/-/tree/main/front)

