(() => {
  "use strict";
  var e = {
      4001: (e, t, n) => {
        n(5363), n(71);
        var a = n(8880),
          r = n(9592),
          o = n(3673);
        function i(e, t, n, a, r, i) {
          const s = (0, o.up)("router-view");
          return (0, o.wg)(), (0, o.j4)(s);
        }
        const s = (0, o.aZ)({ name: "App" });
        var l = n(4260);
        const c = (0, l.Z)(s, [["render", i]]),
          p = c;
        var d = n(4584),
          u = n(3340),
          m = n(4885),
          h = n(9582),
          f = n(5474);
        const g = [
            {
              path: "/",
              component: () =>
                Promise.all([n.e(736), n.e(166)]).then(n.bind(n, 3166)),
              children: [
                {
                  path: "orders",
                  name: "home",
                  component: () =>
                    Promise.all([n.e(736), n.e(616)]).then(n.bind(n, 8616)),
                  beforeEnter: (e, t, n) => {
                    let a = m.Z.getItem("jwt");
                    null === a
                      ? f.api
                          .get("returnSession", {
                            headers: { accept: "application/json" },
                          })
                          .then((e) => {
                            m.Z.set("jwt", e.data.message),
                              (a = m.Z.getItem("jwt")),
                              n();
                          })
                          .catch((e) => {
                            n({ name: "login" });
                          })
                      : n();
                  },
                },
                {
                  path: "mediosPagos",
                  name: "mediosPagos",
                  component: () =>
                    Promise.all([n.e(736), n.e(710)]).then(n.bind(n, 4710)),
                  beforeEnter: async (e, t, n) => {
                    let a = m.Z.getItem("jwt");
                    null === a
                      ? await f.api
                          .get("returnSession", {
                            headers: { accept: "application/json" },
                          })
                          .then((e) => {
                            m.Z.set("jwt", e.data.message),
                              (a = m.Z.getItem("jwt")),
                              n();
                          })
                          .catch((e) => {
                            n({ name: "login" });
                          })
                      : n();
                  },
                },
                {
                  path: "nuevoPedido",
                  name: "nuevoPedido",
                  component: () =>
                    Promise.all([n.e(736), n.e(694)]).then(n.bind(n, 8694)),
                  beforeEnter: async (e, t, n) => {
                    let a = m.Z.getItem("jwt");
                    null === a
                      ? await f.api
                          .get("returnSession", {
                            headers: { accept: "application/json" },
                          })
                          .then((e) => {
                            m.Z.set("jwt", e.data.message),
                              (a = m.Z.getItem("jwt")),
                              n();
                          })
                          .catch((e) => {
                            n({ name: "login" });
                          })
                      : n();
                  },
                },
                {
                  path: "productos",
                  name: "productos",
                  component: () =>
                    Promise.all([n.e(736), n.e(189)]).then(n.bind(n, 189)),
                  beforeEnter: async (e, t, n) => {
                    let a = m.Z.getItem("jwt");
                    null === a
                      ? await f.api
                          .get("returnSession", {
                            headers: { accept: "application/json" },
                          })
                          .then((e) => {
                            m.Z.set("jwt", e.data.message),
                              (a = m.Z.getItem("jwt")),
                              n();
                          })
                          .catch((e) => {
                            n({ name: "login" });
                          })
                      : n();
                  },
                },
                {
                  path: "users",
                  name: "users",
                  component: () =>
                    Promise.all([n.e(736), n.e(556)]).then(n.bind(n, 9556)),
                  beforeEnter: async (e, t, n) => {
                    let a = m.Z.getItem("jwt");
                    null === a
                      ? await f.api
                          .get("returnSession", {
                            headers: { accept: "application/json" },
                          })
                          .then((e) => {
                            m.Z.set("jwt", e.data.message),
                              (a = m.Z.getItem("jwt")),
                              n();
                          })
                          .catch((e) => {
                            n({ name: "login" });
                          })
                      : n();
                  },
                },
                {
                  path: "/orderStates",
                  name: "orderStates",
                  component: () =>
                    Promise.all([n.e(736), n.e(463)]).then(n.bind(n, 1463)),
                  beforeEnter: async (e, t, n) => {
                    let a = m.Z.getItem("jwt");
                    null === a
                      ? await f.api
                          .get("returnSession", {
                            headers: { accept: "application/json" },
                          })
                          .then((e) => {
                            m.Z.set("jwt", e.data.message),
                              (a = m.Z.getItem("jwt")),
                              n();
                          })
                          .catch((e) => {
                            n({ name: "login" });
                          })
                      : n();
                  },
                },
                {
                  path: "/orderGenerate",
                  name: "orderGenerate",
                  component: () =>
                    Promise.all([n.e(736), n.e(933)]).then(n.bind(n, 8933)),
                  beforeEnter: async (e, t, n) => {
                    let a = m.Z.getItem("jwt");
                    null === a
                      ? await f.api
                          .get("returnSession", {
                            headers: { accept: "application/json" },
                          })
                          .then((e) => {
                            m.Z.set("jwt", e.data.message),
                              (a = m.Z.getItem("jwt")),
                              n();
                          })
                          .catch((e) => {
                            n({ name: "login" });
                          })
                      : n();
                  },
                },
                {
                  path: "/payments",
                  name: "payments",
                  component: () =>
                    Promise.all([n.e(736), n.e(361)]).then(n.bind(n, 8361)),
                  beforeEnter: async (e, t, n) => {
                    let a = m.Z.getItem("jwt");
                    null === a
                      ? await f.api
                          .get("returnSession", {
                            headers: { accept: "application/json" },
                          })
                          .then((e) => {
                            m.Z.set("jwt", e.data.message),
                              (a = m.Z.getItem("jwt")),
                              n();
                          })
                          .catch((e) => {
                            n({ name: "login" });
                          })
                      : n();
                  },
                },
                {
                  path: "/payments/create",
                  name: "paymentscreate",
                  component: () =>
                    Promise.all([n.e(736), n.e(372)]).then(n.bind(n, 7372)),
                  beforeEnter: async (e, t, n) => {
                    let a = m.Z.getItem("jwt");
                    null === a
                      ? await f.api
                          .get("returnSession", {
                            headers: { accept: "application/json" },
                          })
                          .then((e) => {
                            m.Z.set("jwt", e.data.message),
                              (a = m.Z.getItem("jwt")),
                              n();
                          })
                          .catch((e) => {
                            n({ name: "login" });
                          })
                      : n();
                  },
                },
              ],
            },
            {
              path: "",
              name: "login",
              component: () =>
                Promise.all([n.e(736), n.e(804)]).then(n.bind(n, 804)),
            },
            {
              path: "/:catchAll(.*)*",
              component: () =>
                Promise.all([n.e(736), n.e(614)]).then(n.bind(n, 5614)),
            },
          ],
          b = g,
          w = (0, u.BC)(function () {
            const e = h.r5,
              t = (0, h.p7)({
                scrollBehavior: () => ({ left: 0, top: 0 }),
                routes: b,
                history: e(""),
              });
            return (
              t.beforeEach((e, t, n) => {
                m.Z.getItem("jwt");
                n();
              }),
              t
            );
          });
        async function v(e, t) {
          const a =
              "function" === typeof d["default"]
                ? await (0, d["default"])({})
                : d["default"],
            { storeKey: o } = await Promise.resolve().then(n.bind(n, 4584)),
            i = "function" === typeof w ? await w({ store: a }) : w;
          a.$router = i;
          const s = e(p);
          return s.use(r.Z, t), { app: s, store: a, storeKey: o, router: i };
        }
        var y = n(4434),
          j = n(499);
        const Z = { config: {}, plugins: { Notify: y.Z, Dialog: j.Z } },
          P = "";
        async function E({ app: e, router: t, store: n, storeKey: a }, r) {
          let o = !1;
          const i = (e) => {
              try {
                return t.resolve(e).href;
              } catch (n) {}
              return Object(e) === e ? null : e;
            },
            s = (e) => {
              if (((o = !0), "string" === typeof e && /^https?:\/\//.test(e)))
                return void (window.location.href = e);
              const t = i(e);
              null !== t &&
                ((window.location.href = t), window.location.reload());
            },
            l = window.location.href.replace(window.location.origin, "");
          for (let p = 0; !1 === o && p < r.length; p++)
            try {
              await r[p]({
                app: e,
                router: t,
                store: n,
                ssrContext: null,
                redirect: s,
                urlPath: l,
                publicPath: P,
              });
            } catch (c) {
              return c && c.url
                ? void s(c.url)
                : void console.error("[Quasar] boot error:", c);
            }
          !0 !== o && (e.use(t), e.use(n, a), e.mount("#q-app"));
        }
        v(a.ri, Z).then((e) =>
          Promise.all([Promise.resolve().then(n.bind(n, 5474))]).then((t) => {
            const n = t
              .map((e) => e.default)
              .filter((e) => "function" === typeof e);
            E(e, n);
          })
        );
      },
      5474: (e, t, n) => {
        n.r(t), n.d(t, { default: () => s, axios: () => o.a, api: () => i });
        var a = n(3340),
          r = n(52),
          o = n.n(r);
        const i = o().create({ baseURL: "https://www.myappresto.tk/api" }),
          s = (0, a.xr)(({ app: e }) => {
            (e.config.globalProperties.$axios = o()),
              (e.config.globalProperties.$api = i);
          });
      },
      4584: (e, t, n) => {
        n.r(t), n.d(t, { default: () => o });
        var a = n(3340),
          r = n(3617);
        const o = (0, a.h)(function () {
          const e = (0, r.MT)({ modules: {}, strict: !1 });
          return e;
        });
      },
    },
    t = {};
  function n(a) {
    var r = t[a];
    if (void 0 !== r) return r.exports;
    var o = (t[a] = { exports: {} });
    return e[a](o, o.exports, n), o.exports;
  }
  (n.m = e),
    (() => {
      var e = [];
      n.O = (t, a, r, o) => {
        if (!a) {
          var i = 1 / 0;
          for (p = 0; p < e.length; p++) {
            for (var [a, r, o] = e[p], s = !0, l = 0; l < a.length; l++)
              (!1 & o || i >= o) && Object.keys(n.O).every((e) => n.O[e](a[l]))
                ? a.splice(l--, 1)
                : ((s = !1), o < i && (i = o));
            if (s) {
              e.splice(p--, 1);
              var c = r();
              void 0 !== c && (t = c);
            }
          }
          return t;
        }
        o = o || 0;
        for (var p = e.length; p > 0 && e[p - 1][2] > o; p--) e[p] = e[p - 1];
        e[p] = [a, r, o];
      };
    })(),
    (() => {
      n.n = (e) => {
        var t = e && e.__esModule ? () => e["default"] : () => e;
        return n.d(t, { a: t }), t;
      };
    })(),
    (() => {
      n.d = (e, t) => {
        for (var a in t)
          n.o(t, a) &&
            !n.o(e, a) &&
            Object.defineProperty(e, a, { enumerable: !0, get: t[a] });
      };
    })(),
    (() => {
      (n.f = {}),
        (n.e = (e) =>
          Promise.all(
            Object.keys(n.f).reduce((t, a) => (n.f[a](e, t), t), [])
          ));
    })(),
    (() => {
      n.u = (e) =>
        "js/" +
        e +
        "." +
        {
          166: "a018fbe2",
          189: "4510da7c",
          361: "22b7673b",
          372: "11aad188",
          463: "0c9d876a",
          556: "5b19be25",
          614: "0f6b003d",
          616: "060e03d7",
          694: "5c55ac4c",
          710: "92e523c8",
          804: "5ad86116",
          933: "751892e9",
        }[e] +
        ".js";
    })(),
    (() => {
      n.miniCssF = (e) =>
        "css/" +
        ({ 143: "app", 736: "vendor" }[e] || e) +
        "." +
        { 143: "31d6cfe0", 189: "8f2aefe0", 616: "de5801e5", 736: "a864fd8a" }[
          e
        ] +
        ".css";
    })(),
    (() => {
      n.g = (function () {
        if ("object" === typeof globalThis) return globalThis;
        try {
          return this || new Function("return this")();
        } catch (e) {
          if ("object" === typeof window) return window;
        }
      })();
    })(),
    (() => {
      n.o = (e, t) => Object.prototype.hasOwnProperty.call(e, t);
    })(),
    (() => {
      var e = {},
        t = "front:";
      n.l = (a, r, o, i) => {
        if (e[a]) e[a].push(r);
        else {
          var s, l;
          if (void 0 !== o)
            for (
              var c = document.getElementsByTagName("script"), p = 0;
              p < c.length;
              p++
            ) {
              var d = c[p];
              if (
                d.getAttribute("src") == a ||
                d.getAttribute("data-webpack") == t + o
              ) {
                s = d;
                break;
              }
            }
          s ||
            ((l = !0),
            (s = document.createElement("script")),
            (s.charset = "utf-8"),
            (s.timeout = 120),
            n.nc && s.setAttribute("nonce", n.nc),
            s.setAttribute("data-webpack", t + o),
            (s.src = a)),
            (e[a] = [r]);
          var u = (t, n) => {
              (s.onerror = s.onload = null), clearTimeout(m);
              var r = e[a];
              if (
                (delete e[a],
                s.parentNode && s.parentNode.removeChild(s),
                r && r.forEach((e) => e(n)),
                t)
              )
                return t(n);
            },
            m = setTimeout(
              u.bind(null, void 0, { type: "timeout", target: s }),
              12e4
            );
          (s.onerror = u.bind(null, s.onerror)),
            (s.onload = u.bind(null, s.onload)),
            l && document.head.appendChild(s);
        }
      };
    })(),
    (() => {
      n.r = (e) => {
        "undefined" !== typeof Symbol &&
          Symbol.toStringTag &&
          Object.defineProperty(e, Symbol.toStringTag, { value: "Module" }),
          Object.defineProperty(e, "__esModule", { value: !0 });
      };
    })(),
    (() => {
      n.p = "";
    })(),
    (() => {
      var e = (e, t, n, a) => {
          var r = document.createElement("link");
          (r.rel = "stylesheet"), (r.type = "text/css");
          var o = (o) => {
            if (((r.onerror = r.onload = null), "load" === o.type)) n();
            else {
              var i = o && ("load" === o.type ? "missing" : o.type),
                s = (o && o.target && o.target.href) || t,
                l = new Error(
                  "Loading CSS chunk " + e + " failed.\n(" + s + ")"
                );
              (l.code = "CSS_CHUNK_LOAD_FAILED"),
                (l.type = i),
                (l.request = s),
                r.parentNode.removeChild(r),
                a(l);
            }
          };
          return (
            (r.onerror = r.onload = o),
            (r.href = t),
            document.head.appendChild(r),
            r
          );
        },
        t = (e, t) => {
          for (
            var n = document.getElementsByTagName("link"), a = 0;
            a < n.length;
            a++
          ) {
            var r = n[a],
              o = r.getAttribute("data-href") || r.getAttribute("href");
            if ("stylesheet" === r.rel && (o === e || o === t)) return r;
          }
          var i = document.getElementsByTagName("style");
          for (a = 0; a < i.length; a++) {
            (r = i[a]), (o = r.getAttribute("data-href"));
            if (o === e || o === t) return r;
          }
        },
        a = (a) =>
          new Promise((r, o) => {
            var i = n.miniCssF(a),
              s = n.p + i;
            if (t(i, s)) return r();
            e(a, s, r, o);
          }),
        r = { 143: 0 };
      n.f.miniCss = (e, t) => {
        var n = { 189: 1, 616: 1 };
        r[e]
          ? t.push(r[e])
          : 0 !== r[e] &&
            n[e] &&
            t.push(
              (r[e] = a(e).then(
                () => {
                  r[e] = 0;
                },
                (t) => {
                  throw (delete r[e], t);
                }
              ))
            );
      };
    })(),
    (() => {
      var e = { 143: 0 };
      (n.f.j = (t, a) => {
        var r = n.o(e, t) ? e[t] : void 0;
        if (0 !== r)
          if (r) a.push(r[2]);
          else {
            var o = new Promise((n, a) => (r = e[t] = [n, a]));
            a.push((r[2] = o));
            var i = n.p + n.u(t),
              s = new Error(),
              l = (a) => {
                if (n.o(e, t) && ((r = e[t]), 0 !== r && (e[t] = void 0), r)) {
                  var o = a && ("load" === a.type ? "missing" : a.type),
                    i = a && a.target && a.target.src;
                  (s.message =
                    "Loading chunk " + t + " failed.\n(" + o + ": " + i + ")"),
                    (s.name = "ChunkLoadError"),
                    (s.type = o),
                    (s.request = i),
                    r[1](s);
                }
              };
            n.l(i, l, "chunk-" + t, t);
          }
      }),
        (n.O.j = (t) => 0 === e[t]);
      var t = (t, a) => {
          var r,
            o,
            [i, s, l] = a,
            c = 0;
          if (i.some((t) => 0 !== e[t])) {
            for (r in s) n.o(s, r) && (n.m[r] = s[r]);
            if (l) var p = l(n);
          }
          for (t && t(a); c < i.length; c++)
            (o = i[c]), n.o(e, o) && e[o] && e[o][0](), (e[o] = 0);
          return n.O(p);
        },
        a = (self["webpackChunkfront"] = self["webpackChunkfront"] || []);
      a.forEach(t.bind(null, 0)), (a.push = t.bind(null, a.push.bind(a)));
    })();
  var a = n.O(void 0, [736], () => n(4001));
  a = n.O(a);
})();
