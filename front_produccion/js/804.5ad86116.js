"use strict";
(self["webpackChunkfront"] = self["webpackChunkfront"] || []).push([
  [804],
  {
    2255: (e, a, l) => {
      l.d(a, { Z: () => y });
      var o = l(3673);
      const t = {
          class: "full-width row wrap justify-center items-start content-start",
        },
        n = { class: "column q-pa-lg" },
        s = (0, o._)(
          "h4",
          { class: "text-h5 text-white q-my-md" },
          "Registro Nuevo Usuario",
          -1
        );
      function r(e, a, l, r, i, d) {
        const u = (0, o.up)("q-card-section"),
          m = (0, o.up)("q-icon"),
          p = (0, o.up)("q-input"),
          c = (0, o.up)("q-form"),
          g = (0, o.up)("q-btn"),
          w = (0, o.up)("q-card-actions"),
          h = (0, o.up)("q-card");
        return (
          (0, o.wg)(),
          (0, o.iD)("div", t, [
            (0, o._)("div", n, [
              (0, o.Wm)(
                h,
                {
                  square: "",
                  class: "shadow-24",
                  style: { width: "300px", height: "100%" },
                },
                {
                  default: (0, o.w5)(() => [
                    (0, o.Wm)(
                      u,
                      { class: "bg-deep-purple-7" },
                      { default: (0, o.w5)(() => [s]), _: 1 }
                    ),
                    (0, o.Wm)(u, null, {
                      default: (0, o.w5)(() => [
                        (0, o.Wm)(
                          c,
                          { class: "q-px-sm q-pt-xl q-pb-lg" },
                          {
                            default: (0, o.w5)(() => [
                              (0, o.Wm)(
                                p,
                                {
                                  square: "",
                                  clearable: "",
                                  modelValue: e.mail,
                                  "onUpdate:modelValue":
                                    a[0] || (a[0] = (a) => (e.mail = a)),
                                  type: "email",
                                  label: "Email",
                                },
                                {
                                  prepend: (0, o.w5)(() => [
                                    (0, o.Wm)(m, { name: "email" }),
                                  ]),
                                  _: 1,
                                },
                                8,
                                ["modelValue"]
                              ),
                              (0, o.Wm)(
                                p,
                                {
                                  square: "",
                                  clearable: "",
                                  modelValue: e.user,
                                  "onUpdate:modelValue":
                                    a[1] || (a[1] = (a) => (e.user = a)),
                                  type: "username",
                                  label: "Username",
                                },
                                {
                                  prepend: (0, o.w5)(() => [
                                    (0, o.Wm)(m, { name: "person" }),
                                  ]),
                                  _: 1,
                                },
                                8,
                                ["modelValue"]
                              ),
                              (0, o.Wm)(
                                p,
                                {
                                  square: "",
                                  clearable: "",
                                  modelValue: e.name_lastName,
                                  "onUpdate:modelValue":
                                    a[2] ||
                                    (a[2] = (a) => (e.name_lastName = a)),
                                  type: "name_lastName",
                                  label: "name_lastName",
                                },
                                {
                                  prepend: (0, o.w5)(() => [
                                    (0, o.Wm)(m, { name: "person" }),
                                  ]),
                                  _: 1,
                                },
                                8,
                                ["modelValue"]
                              ),
                              (0, o.Wm)(
                                p,
                                {
                                  square: "",
                                  clearable: "",
                                  modelValue: e.telephone,
                                  "onUpdate:modelValue":
                                    a[3] || (a[3] = (a) => (e.telephone = a)),
                                  type: "telephone",
                                  label: "telephone",
                                },
                                {
                                  prepend: (0, o.w5)(() => [
                                    (0, o.Wm)(m, { name: "phone_android" }),
                                  ]),
                                  _: 1,
                                },
                                8,
                                ["modelValue"]
                              ),
                              (0, o.Wm)(
                                p,
                                {
                                  square: "",
                                  clearable: "",
                                  modelValue: e.shipping_address,
                                  "onUpdate:modelValue":
                                    a[4] ||
                                    (a[4] = (a) => (e.shipping_address = a)),
                                  type: "shipping_address",
                                  label: "shipping_address",
                                },
                                {
                                  prepend: (0, o.w5)(() => [
                                    (0, o.Wm)(m, { name: "home" }),
                                  ]),
                                  _: 1,
                                },
                                8,
                                ["modelValue"]
                              ),
                              (0, o.Wm)(
                                p,
                                {
                                  square: "",
                                  clearable: "",
                                  modelValue: e.password,
                                  "onUpdate:modelValue":
                                    a[5] || (a[5] = (a) => (e.password = a)),
                                  type: "password",
                                  label: "Password",
                                },
                                {
                                  prepend: (0, o.w5)(() => [
                                    (0, o.Wm)(m, { name: "lock" }),
                                  ]),
                                  _: 1,
                                },
                                8,
                                ["modelValue"]
                              ),
                            ]),
                            _: 1,
                          }
                        ),
                      ]),
                      _: 1,
                    }),
                    (0, o.Wm)(
                      w,
                      { class: "q-px-lg" },
                      {
                        default: (0, o.w5)(() => [
                          (0, o.Wm)(
                            g,
                            {
                              unelevated: "",
                              size: "lg",
                              color: "purple-4",
                              class: "full-width text-white",
                              label: "Guardar",
                              onClick: e.addUser,
                            },
                            null,
                            8,
                            ["onClick"]
                          ),
                        ]),
                        _: 1,
                      }
                    ),
                  ]),
                  _: 1,
                }
              ),
            ]),
          ])
        );
      }
      l(2100);
      var i = l(1959),
        d = l(5474),
        u = l(8825);
      l(4885);
      const m = (0, o.aZ)({
        name: "AddUser",
        setup() {
          const e = (0, u.Z)(),
            a = (0, i.iH)(null),
            l = (0, i.iH)(null),
            o = (0, i.iH)(null),
            t = (0, i.iH)(null),
            n = (0, i.iH)(null),
            s = (0, i.iH)(null),
            r = (0, i.iH)(null),
            m = (0, i.iH)(null);
          return {
            async addUser() {
              const r = {
                usuario: l.value,
                nombre_y_apellido: t.value,
                correo_electronico: a.value,
                telefono: n.value,
                direccion_de_envio: s.value,
                contrasena: o.value,
                perfil: "admin",
              };
              console.log(`user ${JSON.stringify(r)}`),
                await d.api
                  .post("nuevo", r, { headers: { accept: "application/json" } })
                  .then(
                    (a) => (
                      console.log("datos " + JSON.stringify(a.data)),
                      e.notify({
                        color: "positive",
                        position: "bottom",
                        message: `Se creo el usuario ${l.value}`,
                        icon: "mood",
                      }),
                      JSON.stringify(a.data)
                    )
                  )
                  .catch((a) => {
                    e.notify({
                      color: "negative",
                      position: "bottom",
                      message: "No se pudo guardar el nuevo usuario",
                      icon: "sentiment_very_dissatisfied",
                    }),
                      console.log("error", a);
                  }),
                console.log(m);
            },
            datos: m,
            mail: a,
            user: l,
            password: o,
            name_lastName: t,
            telephone: n,
            shipping_address: s,
            profile: r,
          };
        },
      });
      var p = l(4260),
        c = l(151),
        g = l(5589),
        w = l(5269),
        h = l(4842),
        _ = l(4554),
        f = l(9367),
        b = l(8240),
        q = l(7518),
        W = l.n(q);
      const v = (0, p.Z)(m, [["render", r]]),
        y = v;
      W()(m, "components", {
        QCard: c.Z,
        QCardSection: g.Z,
        QForm: w.Z,
        QInput: h.Z,
        QIcon: _.Z,
        QCardActions: f.Z,
        QBtn: b.Z,
      });
    },
    804: (e, a, l) => {
      l.r(a), l.d(a, { default: () => S });
      var o = l(3673);
      const t = { class: "q-pa-md" },
        n = {
          class: "q-gutter-y-md",
          style: { "max-width": "100%", "max-height": "100px" },
        },
        s = {
          class: "full-width row wrap justify-center items-start content-start",
        },
        r = { class: "column q-pa-lg" },
        i = (0, o._)(
          "h4",
          { class: "text-h5 text-white q-my-md" },
          " Restaurant Delilah ",
          -1
        ),
        d = { class: "text-center q-pa-md q-gutter-md" };
      function u(e, a, l, u, m, p) {
        const c = (0, o.up)("q-tab"),
          g = (0, o.up)("q-tabs"),
          w = (0, o.up)("q-separator"),
          h = (0, o.up)("q-card-section"),
          _ = (0, o.up)("q-icon"),
          f = (0, o.up)("q-input"),
          b = (0, o.up)("q-form"),
          q = (0, o.up)("q-btn"),
          W = (0, o.up)("q-card-actions"),
          v = (0, o.up)("q-card"),
          y = (0, o.up)("q-tab-panel"),
          V = (0, o.up)("AddUser"),
          Z = (0, o.up)("q-tab-panels");
        return (
          (0, o.wg)(),
          (0, o.iD)("div", t, [
            (0, o._)("div", n, [
              (0, o.Wm)(v, null, {
                default: (0, o.w5)(() => [
                  (0, o.Wm)(
                    g,
                    {
                      modelValue: u.tab,
                      "onUpdate:modelValue":
                        a[0] || (a[0] = (e) => (u.tab = e)),
                      dense: "",
                      class: "text-grey",
                      "active-color": "primary",
                      "indicator-color": "primary",
                      align: "justify",
                      "narrow-indicator": "",
                    },
                    {
                      default: (0, o.w5)(() => [
                        (0, o.Wm)(c, { name: "login", label: "Login" }),
                        (0, o.Wm)(c, {
                          name: "addUser",
                          label: "Nuevo Usuario",
                        }),
                      ]),
                      _: 1,
                    },
                    8,
                    ["modelValue"]
                  ),
                  (0, o.Wm)(w),
                  (0, o.Wm)(
                    Z,
                    {
                      modelValue: u.tab,
                      "onUpdate:modelValue":
                        a[3] || (a[3] = (e) => (u.tab = e)),
                      animated: "",
                      align: "center",
                    },
                    {
                      default: (0, o.w5)(() => [
                        (0, o.Wm)(
                          y,
                          { name: "login" },
                          {
                            default: (0, o.w5)(() => [
                              (0, o._)("div", s, [
                                (0, o._)("div", r, [
                                  (0, o.Wm)(
                                    v,
                                    {
                                      square: "",
                                      class: "shadow-24",
                                      style: {
                                        width: "300px",
                                        height: "525px",
                                      },
                                    },
                                    {
                                      default: (0, o.w5)(() => [
                                        (0, o.Wm)(
                                          h,
                                          { class: "bg-deep-purple-7" },
                                          {
                                            default: (0, o.w5)(() => [i]),
                                            _: 1,
                                          }
                                        ),
                                        (0, o.Wm)(h, null, {
                                          default: (0, o.w5)(() => [
                                            (0, o.Wm)(
                                              b,
                                              { class: "q-px-sm q-pt-xl" },
                                              {
                                                default: (0, o.w5)(() => [
                                                  (0, o.Wm)(
                                                    f,
                                                    {
                                                      square: "",
                                                      clearable: "",
                                                      modelValue: u.email,
                                                      "onUpdate:modelValue":
                                                        a[1] ||
                                                        (a[1] = (e) =>
                                                          (u.email = e)),
                                                      type: "email",
                                                      label: "Email",
                                                    },
                                                    {
                                                      prepend: (0, o.w5)(() => [
                                                        (0, o.Wm)(_, {
                                                          name: "email",
                                                        }),
                                                      ]),
                                                      _: 1,
                                                    },
                                                    8,
                                                    ["modelValue"]
                                                  ),
                                                  (0, o.Wm)(
                                                    f,
                                                    {
                                                      square: "",
                                                      clearable: "",
                                                      modelValue: u.password,
                                                      "onUpdate:modelValue":
                                                        a[2] ||
                                                        (a[2] = (e) =>
                                                          (u.password = e)),
                                                      type: "password",
                                                      label: "Contraseña",
                                                      rules: [
                                                        (e) =>
                                                          (e && e.length > 0) ||
                                                          "Debe ingresar al menos un caracter",
                                                      ],
                                                    },
                                                    {
                                                      prepend: (0, o.w5)(() => [
                                                        (0, o.Wm)(_, {
                                                          name: "lock",
                                                        }),
                                                      ]),
                                                      _: 1,
                                                    },
                                                    8,
                                                    ["modelValue", "rules"]
                                                  ),
                                                ]),
                                                _: 1,
                                              }
                                            ),
                                          ]),
                                          _: 1,
                                        }),
                                        (0, o.Wm)(h, null, {
                                          default: (0, o.w5)(() => [
                                            (0, o._)("div", d, [
                                              (0, o.Wm)(
                                                q,
                                                { round: "", color: "red-8" },
                                                {
                                                  default: (0, o.w5)(() => [
                                                    (0, o.Wm)(
                                                      _,
                                                      {
                                                        name: "img:/icons/google.png",
                                                        size: "40px",
                                                        onClick: u.google,
                                                      },
                                                      null,
                                                      8,
                                                      ["onClick"]
                                                    ),
                                                  ]),
                                                  _: 1,
                                                }
                                              ),
                                              (0, o.Wm)(
                                                q,
                                                {
                                                  round: "",
                                                  color: "light-blue-5",
                                                },
                                                {
                                                  default: (0, o.w5)(() => [
                                                    (0, o.Wm)(
                                                      _,
                                                      {
                                                        name: "img:/icons/linkedin.png",
                                                        size: "40px",
                                                        onClick: u.linkedin,
                                                      },
                                                      null,
                                                      8,
                                                      ["onClick"]
                                                    ),
                                                  ]),
                                                  _: 1,
                                                }
                                              ),
                                            ]),
                                          ]),
                                          _: 1,
                                        }),
                                        (0, o.Wm)(
                                          W,
                                          { class: "q-px-lg" },
                                          {
                                            default: (0, o.w5)(() => [
                                              (0, o.Wm)(
                                                q,
                                                {
                                                  unelevated: "",
                                                  size: "lg",
                                                  color: "purple-4",
                                                  class:
                                                    "full-width text-white",
                                                  label: "Ingresar",
                                                  onClick: u.login,
                                                },
                                                null,
                                                8,
                                                ["onClick"]
                                              ),
                                            ]),
                                            _: 1,
                                          }
                                        ),
                                      ]),
                                      _: 1,
                                    }
                                  ),
                                ]),
                              ]),
                            ]),
                            _: 1,
                          }
                        ),
                        (0, o.Wm)(
                          y,
                          { name: "addUser" },
                          {
                            default: (0, o.w5)(() => [
                              (0, o.Wm)(V, { register: u.register }, null, 8, [
                                "register",
                              ]),
                            ]),
                            _: 1,
                          }
                        ),
                      ]),
                      _: 1,
                    },
                    8,
                    ["modelValue"]
                  ),
                ]),
                _: 1,
              }),
            ]),
          ])
        );
      }
      var m = l(1959),
        p = l(5474),
        c = l(8825),
        g = l(4885),
        w = l(2255),
        h = l(9582);
      const _ = {
        name: "Login",
        setup() {
          const e = (0, h.tv)(),
            a = (0, c.Z)(),
            l = (0, m.iH)(!1),
            o = (0, m.iH)(null),
            t = (0, m.iH)(null),
            n = (0, m.iH)(null),
            s = (0, m.iH)(null);
          return {
            async login() {
              const l = { usuario: o.value, contrasena: n.value };
              await p.api
                .post("/login", l, { headers: { accept: "application/json" } })
                .then(
                  (a) => (
                    g.Z.set("jwt", a.data.message), e.push({ name: "home" })
                  )
                )
                .catch((e) => {
                  a.notify({
                    color: "negative",
                    position: "bottom",
                    message: "Usuario o contraseña incorrecto",
                    icon: "sentiment_very_dissatisfied",
                  });
                });
            },
            async google() {
              g.Z.remove("jwt"),
                (location.href = "https://www.myappresto.tk/api/google");
            },
            async linkedin() {
              g.Z.remove("jwt"),
                (location.href = "https://www.myappresto.tk/api/linkedin");
            },
            datos: s,
            register: l,
            email: o,
            username: t,
            password: n,
            tab: (0, m.iH)("login"),
            splitterModel: (0, m.iH)(20),
          };
        },
        components: { AddUser: w.Z },
      };
      var f = l(4260),
        b = l(151),
        q = l(7547),
        W = l(3269),
        v = l(5869),
        y = l(5906),
        V = l(6602),
        Z = l(5589),
        x = l(5269),
        U = l(4842),
        Q = l(4554),
        k = l(8240),
        C = l(9367),
        H = l(7518),
        N = l.n(H);
      const j = (0, f.Z)(_, [["render", u]]),
        S = j;
      N()(_, "components", {
        QCard: b.Z,
        QTabs: q.Z,
        QTab: W.Z,
        QSeparator: v.Z,
        QTabPanels: y.Z,
        QTabPanel: V.Z,
        QCardSection: Z.Z,
        QForm: x.Z,
        QInput: U.Z,
        QIcon: Q.Z,
        QBtn: k.Z,
        QCardActions: C.Z,
      });
    },
  },
]);
