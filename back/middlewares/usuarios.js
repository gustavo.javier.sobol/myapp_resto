require("dotenv").config();
// traigo el array de datos de los usuarios
const varUsuario = require("../models/usuario.js");
const storage = require("node-sessionstorage");
const jwt = require("jsonwebtoken");

// chequeo de token para verificar si existe
const checkToken = (req, res) => {
  if (
    req.headers.authorization === "undefined" ||
    req.headers.authorization === "null" ||
    req.headers.authorization === undefined
  ) {
    if (storage.getItem("jwt") === undefined) {
      // return `No hay ningún token ingresado`;
      return res.status(401).send({
        message: "Unable to find data 1 ",
        status: 401,
      });
    } else {
      // hay que resolver y ver unicamente los casos que necesita el token
      // desde el back ya que cualquiera puede acceder si la pagina tiene un token
      req.headers.authorization = storage.getItem("jwt");
      tokenIngress = req.headers.authorization;
      //storage.removeItem("jwt");
    }
  } else {
    tokenIngress = req.headers.authorization;
  } // || varUsuario.token;
  const token = tokenIngress.replace("Bearer ", "");

  if (token === "") return res.json("Error token vacio");

  return jwt.verify(token, process.env.JWT_SECRET, function (err, decoded) {
    if (err) {
      if (err === "TokenExpiredError: jwt expired") {
        res.status(301).redirect("/logout");
      } else {
        return res.status(401).send({
          message: "Unable to find data 1 ",
          errors: err,
          status: 401,
        });
      }
    } else {
      // Con este codigo es para un solo usuario
      /* if (token === varUsuario.token) {
        return decoded;
      } else {
        res.status(401).send({
          message: "Unable to find data 2 ",
          status: 401,
        });
      } */
      varUsuario.usuarioLogueado = decoded.dataUser;

      return decoded;
      // return `Token invalido: ${token}`;
    }
  });
};

const userLog = async (req, res, next) => {
  const userToken = await checkToken(req, res);
  if (userToken === undefined) {
    return res.json({
      message: `${userToken}`,
      status: 401,
    });
  } else if (typeof userToken === "object") {
    next();
  } else {
    return res.json({
      message: `${userToken}`,
      status: 200,
    });
  }
};

const userAdmin = (req, res, next) => {
  const userToken = checkToken(req, res);
  userToken.dataUser.profile === "admin"
    ? next()
    : res.json(`Su usuario no posee permisos para realizar modificaciones`);
};

const postCallBack = async (req, res, next) => {
  await checkToken(req, res).then((response) => {
    res.status(301).redirect(`http://${process.env.HOST}/#/`);
    next();
  });
};

module.exports = { postCallBack, userLog, userAdmin };
