require("dotenv").config();
const assert = require("assert");
const fetch = require("node-fetch");
const url = `http://${process.env.HOST}/`;
const newUser = {
  usuario: "test",
  nombre_y_apellido: "test",
  correo_electronico: "test_user@gmail.com",
  telefono: 11112,
  direccion_de_envio: "calle sin nombre 557",
  contrasena: "555555",
};
const urlNewUser = url + "nuevo";
describe(`Test User`, () => {
  it(`Add new user - Request 200`, async () => {
    await fetch(urlNewUser, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newUser),
    })
      .then((response) => response.json())
      .then((data) => {
        assert.equal(data.status, 200);
      });
  });
  it(`User exists - Request 404 `, async () => {
    await fetch(urlNewUser, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newUser),
    })
      .then((response) => response.json())
      .then((data) => {
        assert.equal(data.status, 404);
      });
  });
});
