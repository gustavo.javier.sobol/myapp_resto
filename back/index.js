require("dotenv").config();
const express = require("express");
const app = express();
const helmet = require("helmet");
const cors = require("cors");
const sequelize = require("./config/db.config");
const passportGoogle = require("./config/passport-google");
const passportLinkedin = require("./config/passport-linkedin");

require("./models/asociations");

const passport = require("passport");
const session = require("express-session");
app.use(session({ secret: "1234", resave: false, saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());
// require("./models/asociaciones");

// configuraciones iniciales
const port = process.env.PORT || 4000;
const host = process.env.HOST || "localhost";
app.use(express.json());

// importacion de middlewes
const midUsers = require("./middlewares/usuarios");

// configuraciones swagger
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUI = require("swagger-ui-express");
const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: "API - Delilah Restó",
      version: "2.0.0",
      description:
        "Sistema de pedidos de comida \n Para la empresa Delilah Restó \n Una api desarrollada por Sobol Gustavo de Globant ",
      contact: {
        name: "Sobol Gustavo",
        url: "http://localhost:4000/",
        email: "gustavo.javier.sobol@gmail.com",
      },
    },
    securityDefinitions: {
      jwt: {
        type: "apiKey",
        description: "",
        name: "Authorization",
        in: "headers",
      },
    },
  },

  apis: [
    "./index.js",
    "./routes/login.js",
    "./routes/pedidos.js",
    "./routes/usuarios.js",
    "./routes/productos.js",
    "./routes/mediosPago.js",
    "./routes/pagos.js",
  ],
};
app.use(cors());

const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerDocs));
app.use(helmet());

// configuracion de mercado pago
const mecardopago = require("./routes/mecardopago");
app.use("/mecardopago", mecardopago);

// configuraciones de usuarios
const login = require("./routes/login");
app.use("/", login);

const usuarios = require("./routes/usuarios");
// [midUsers.userLog, midUsers.userAdmin],
app.use("/usuarios", [midUsers.userLog, midUsers.userAdmin], usuarios);

// configuraciones generales productos
var productos = require("./routes/productos");
app.use("/productos", [midUsers.userLog], productos);

// configuraciones generales pedidos
var pedidos = require("./routes/pedidos");
app.use("/pedidos", midUsers.userLog, pedidos);

// configuraciones generales Medios de pagos
var mediosPago = require("./routes/mediosPago");
app.use("/mediosPago", midUsers.userLog, mediosPago);

app.listen(port, () => {
  // {force: true,alter: true }
  //sequelize.sync( /* {alter: true } */).then(() => console.log("Database connect"));
  sequelize.authenticate().then(() => {
    console.log("Database connect");
  });
  console.log(`Servidor corriendo en http://${host}:${port}/api-docs`);
});
