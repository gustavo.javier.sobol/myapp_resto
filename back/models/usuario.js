 let usuarios = [
  {
    usuario: "admin",
    nombre_y_apellido: "Administrador",
    correo_electronico: "admin@restoBar.com",
    telefono: 4,
    direccion_de_envio: "Calle sin nombre 50",
    contrasena: 123456,
    perfil: "admin",
  },
  {
    usuario: "pedidor",
    nombre_y_apellido: "Pedidor",
    correo_electronico: "pedidor@restoBar.com",
    telefono: 5,
    direccion_de_envio: "Calle sin nombre 50",
    contrasena: 123456,
    perfil: "pedidor"
  },
]; 
let usuarioLogueado = [];
let token = "";
module.exports = {usuarios, usuarioLogueado, token } ;


