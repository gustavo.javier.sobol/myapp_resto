const { Model, DataTypes } = require('sequelize');
const sequelize = require("../config/db.config");
class paymentMethods extends Model { }
paymentMethods.init({
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  kind: {
    type: DataTypes.STRING(100)
  }
},
  {
    sequelize,
    modelName: "payment_methods",
    timestamps: false
  });

module.exports = paymentMethods;
