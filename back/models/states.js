let estados = [
  {
    id: 1,
    estado: "Pendiente",
    detalle: "cuando el usuario inicia su pedido.",
  },
  {
    id: 2,
    estado: "Confirmado",
    detalle: "cuando el usuario cierra su pedido.",
  },
  {
    id: 3,
    estado: "Preparación",
    detalle: "cuando el administrador comienza a preparar el pedido.",
  },
  {
    id: 4,
    estado: "Enviado",
    detalle: "cuando el administrador envía el pedido.",
  },
  {
    id: 5,
    estado: "Cancelado",
    detalle: "cuando el usuario cancela su pedido.",
  },
  {
    id: 6,
    estado: "Entregado",
    detalle: "cuando el administrador entrega el pedido.",
  },
];

module.exports = { estados };
