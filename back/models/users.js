const { Model, DataTypes } = require("sequelize");
const sequelize = require("../config/db.config");
class user extends Model {}
user.init(
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    user: {
      type: DataTypes.STRING,
    },
    name_lastName: {
      type: DataTypes.STRING,
    },
    mail: {
      type: DataTypes.STRING,
    },
    telephone: {
      type: DataTypes.STRING,
    },
    shipping_address: {
      type: DataTypes.STRING,
    },
    password: {
      type: DataTypes.STRING.BINARY,
    },
    profile: {
      type: DataTypes.STRING,
    },
    discontinued: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    idGoogle: {
      type: DataTypes.STRING,
    },
    idLinkedin: {
      type: DataTypes.STRING,
    },
  },
  {
    sequelize,
    modelName: "users",
    timestamps: false,
  }
);

module.exports = user;

/* 
const userModel = (connection, Sequelize) => {
  const user = connection.define('users', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      user: {
        type: Sequelize.STRING
      },
      name_lastName: {
        type: Sequelize.STRING
      },
      mail: {
        type: Sequelize.STRING
      },
      telephone: {
        type: Sequelize.INTEGER,
      },
      shipping_address: {
        type: Sequelize.STRING
      },
      password: {
        type: Sequelize.STRING
      },
      profile: {
        type: Sequelize.STRING
      }
  },
  {
    timestamps: false
  });
  return user
};

module.exports =  userModel; */
