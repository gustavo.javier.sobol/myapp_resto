const Order = require('../models/pedidos');
const PaymentMethods = require('../models/mediosPago');
const OrdersHasProducts = require('../models/orders_has_products');
const Product = require('../models/productos');
const User = require('../models/users');

// Uno a uno

// Usuario tiene una direccion
// añadir una clave foranea userId a la tabla addresses
// User.hasOne(Address, { as: "domicilio", foreignKey: "residente_id" });

// Uno a muchos, 1 a N
// Usuario va a tener muchos posts o publicaciones
// Se añade una clave userId a la tabla posts
PaymentMethods.hasMany(Order, { as: "pagos", foreignKey: "payment_methods_id" });
User.hasMany(Order, { as: "order_users", foreignKey: "users_id" });
// Se añade una clave userId a la tabla posts

//Order.hasMany(OrdersHasProducts, { as: "list_order", foreignKey: "orders_id" });
Order.belongsTo(PaymentMethods, { as: "iPayTheOrder", foreignKey: "payment_methods_id" });
Order.belongsTo(User, { as: "client", foreignKey: "users_id" });

//Muchos a muchos
Order.belongsToMany(Product,{ 
    onDelete: 'RESTRICT', 
    onUpdate: 'RESTRICT', 
    through: OrdersHasProducts, 
    foreignKey:'orders_id' 
}); 
Product.belongsToMany(Order,{ 
    onDelete: 'RESTRICT', 
    onUpdate: 'RESTRICT', 
    through: OrdersHasProducts, 
    foreignKey:'products_id' 
}); 