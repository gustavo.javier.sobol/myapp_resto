const { Model, DataTypes } = require('sequelize');
const sequelize = require("../config/db.config");

class ordersHasProducts extends Model { }

ordersHasProducts.init({
      orders_id: {
        type: DataTypes.INTEGER,
        primaryKey: true
      },
      products_id: {
        type: DataTypes.INTEGER,
        primaryKey: true
      },
      ordered_quantity: {
        type: DataTypes.INTEGER
      },
      unit_price: {
        type: DataTypes.INTEGER
      },
      line_price: {
        type: DataTypes.INTEGER
      },
      observations: {
        type: DataTypes.STRING
      }
  },
  {
    sequelize,
    modelName: "orders_has_products",
    timestamps: false
  });

module.exports = ordersHasProducts;








