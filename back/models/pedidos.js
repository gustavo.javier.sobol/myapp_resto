const { Model, DataTypes } = require('sequelize');
const sequelize = require("../config/db.config");

class orders extends Model { }

orders.init({
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  state_id: {
    type: DataTypes.INTEGER
  },
  application_date: {
    type: DataTypes.DATE(6)
  },
  request_time: {
    type: DataTypes.STRING
  },
  shipping_address: {
    type: DataTypes.STRING
  },
  total_pay: {
    type: DataTypes.INTEGER
  },
  users_id: {
    type: DataTypes.INTEGER
  },
  /*   payment_methods_id: {
      type: DataTypes.INTEGER
    }  */
}, {
  sequelize,
  modelName: "orders",
  timestamps: false
});

module.exports = orders;








/*

const orderModel = (connection, Sequelize) => {
  const order = connection.define('orders', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
      state_id: {
        type: Sequelize.INTEGER
      },
      application_date: {
        type: Sequelize.DATE(6)
      },
      request_time: {
        type: Sequelize.STRING
      },
      shipping_address: {
        type: Sequelize.STRING
      },
      total_pay: {
        type: Sequelize.INTEGER
      },
      users_id: {
        type: Sequelize.INTEGER
      },
      payment_methods_id: {
        type: Sequelize.INTEGER
      }

  },
  {
    timestamps: false
  });
  return order
}

module.exports = orderModel;



 */




