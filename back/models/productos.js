const { Model, DataTypes } = require('sequelize');
const sequelize = require("../config/db.config");
class product extends Model { }
product.init({
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  description: {
    type: DataTypes.STRING
  },
  price: {
    type: DataTypes.INTEGER,
  }
},
  {
    sequelize,
    modelName: "products",
    timestamps: false
  });

module.exports = product;
