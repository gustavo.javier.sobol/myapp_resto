require("dotenv").config();
const Sequelize = require("sequelize");
// const { all } = require("sequelize/types/lib/operators");
const connection = require("../config/db.config");
//conecto con el modelo para traer los datos
const productModel = require("../models/productos");

const redis = require("redis");
const bluebird = require("bluebird");
bluebird.promisifyAll(redis);

const client = redis.createClient({
  host: process.env.REDIS_HOST,
  port: process.env.REDIS_PORT,
});

client.on("error", function (error) {
  console.error(error);
});

const refreshAllProducts = () => {
  client.del("products", function (err, response) {
    if (response == 1) {
      productModel
        .findAll()
        .then((res) => {
          client.set("products", JSON.stringify(res), "EX", 60 * 20);
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      console.log("Cannot delete");
    }
  });
};

// funcion devuelve lista con productos
const get = async () => {
  const productsAll = await client.getAsync("products");

  if (productsAll !== null) {
    return JSON.parse(productsAll);
  } else {
    const result = productModel
      .findAll()
      .then((res) => {
        client.set("products", JSON.stringify(res), "EX", 60 * 20);
        return res;
      })
      .catch((err) => {
        return err;
      });
    return result;
  }
};

// busqueda interna del producto si existe
let buscoProducto = async (idProduct) => {
  // devuelve el index del id del producto si no lo encuentra devuelve -1
  return await productModel
    .findOne({ where: { id: idProduct } })
    .then((resultado) => {
      if (resultado) {
        return resultado;
      }
      throw "product does not exist";
    });
};

// funcion para agregar un nuevo producto
const add = async (req) => {
  const newProduct = await productModel.build({
    description: req.body.descripcion,
    price: req.body.precio,
  });
  const result = await newProduct.save();
  refreshAllProducts();
  return result;
};

// funcion para actualizar un producto
const update = async (req) => {
  const idProduct = parseInt(req.params.id, 10);
  return await productModel
    .findOne({ where: { id: idProduct } })
    .then((resultado) => {
      if (resultado) {
        productModel.update(
          {
            description: req.body.descripcion,
            price: req.body.precio,
          },
          { where: { id: idProduct } }
        );
        refreshAllProducts();
        return resultado;
      }
      throw "product does not exist";
    })
    .catch((error) => {
      return error;
    });
};

const delet = async (req) => {
  const idProduct = parseInt(req.params.id, 10);
  const result = await productModel.destroy({
    where: { id: idProduct },
  });
  refreshAllProducts();
  return result;
};

module.exports = { get, add, update, delet, buscoProducto };
