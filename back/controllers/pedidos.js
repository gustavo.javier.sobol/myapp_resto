const { Op } = require("sequelize");
// traigo el array de datos de los usuarios
const varUsuario = require("../models/usuario.js");
// traigo el array de datos de los productos
const varProducto = require("../controllers/productos");
const pedidoModel = require("../models/pedidos");
const OrdersHasProducts = require("../models/orders_has_products");
const product = require("../models/productos");
const PaymentMethods = require("../models/mediosPago");
const Users = require("../models/users");
const userLogin = require("../models/usuario");

let get = async () => {
  return await pedidoModel.findAll({
    include: [
      {
        model: PaymentMethods,
        as: "iPayTheOrder",
        attributes: ["kind"],
      },
      {
        model: Users,
        as: "client",
        attributes: ["user", "name_lastName", "telephone"],
      },
      {
        model: product,
        //as: "pago_la_orden",
        attributes: ["description"],
        through: {
          attributes: ["ordered_quantity", "unit_price", "line_price"],
        },
      },
    ],
  });
};

// funcion devuelve lista con pedidos de un usuario
let getId = async (id) => {
  return await pedidoModel
    .findOne({
      where: { id: id },
    })
    .then((resultado) => {
      if (resultado) {
        return resultado;
      }
      throw "Order no exists";
    });
};
// funcion devuelve lista con pedidos de un usuario
let getUser = async () => {
  let idUser = varUsuario.usuarioLogueado.id;
  return await pedidoModel
    .findAll({
      include: [
        {
          model: PaymentMethods,
          as: "iPayTheOrder",
          attributes: ["kind"],
        },
        {
          model: Users,
          as: "client",
          attributes: ["user", "name_lastName", "telephone"],
        },
        {
          model: product,
          //as: "pago_la_orden",
          attributes: ["description"],
          through: {
            attributes: ["ordered_quantity", "unit_price", "line_price"],
          },
        },
      ],
      where: { users_id: idUser },
    })
    .then((resultado) => {
      if (resultado) {
        return resultado;
      }
      throw "Order no exists";
    });
};

// funcion que busca el Pedido y si el pedido pertenece al usuario
// exceptuando que sea administrador y va a poder ver y modificar todos los pedidos
let buscoPedido = async (idOrder) => {
  // busco por sequelize
  return await pedidoModel
    .findOne({
      where: { id: idOrder },
    })
    .then((resultado) => {
      if (resultado) {
        return resultado;
      } else {
        throw "Order no exists";
      }
    });
};

// funcion para agregar un nuevo producto
const add = async (req) => {
  console.log(`decoder ${JSON.stringify(varUsuario.usuarioLogueado)} 
  and id ${varUsuario.usuarioLogueado.id} `);
  let direccion = varUsuario.usuarioLogueado.shipping_address;
  if (req.body.direccion_envio !== undefined) {
    direccion = req.body.direccion_envio;
  }
  // fecha de hoy en el formato
  let f = new Date();
  const newPedido = await pedidoModel.build({
    state_id: 1,
    // asigno fecha del pedido
    application_date: `${f.getFullYear()}-${f.getMonth() + 1}-${f.getDate()}`,
    // asigno hora del pedido
    request_time: `${f.getHours()}:${f.getMinutes()}`,
    shipping_address: direccion,
    total_pay: 0,
    users_id: varUsuario.usuarioLogueado.id,
    payment_methods_id: req.body.medio_pago,
  });
  const result = await newPedido.save();

  return result;
};

const update = async (req) => {
  const idPedido = parseInt(req.params.id, 10);
  let direccion = varUsuario.usuarioLogueado.shipping_address;
  if (req.body.direccion_envio !== undefined) {
    direccion = req.body.direccion_envio;
  }
  // fecha de hoy en el formato
  let f = new Date();

  return await pedidoModel
    .findOne({ where: { id: idPedido } })
    .then((resultado) => {
      if (resultado) {
        return pedidoModel.update(
          {
            state_id: 1,
            // asigno fecha del pedido
            application_date: `${f.getFullYear()}-${
              f.getMonth() + 1
            }-${f.getDate()}`,
            // asigno hora del pedido
            request_time: `${f.getHours()}:${f.getMinutes()}`,
            shipping_address: direccion,
            total_pay: 0,
            users_id: varUsuario.usuarioLogueado.id,
            payment_methods_id: req.body.medio_pago,
          },
          { where: { id: idPedido } }
        );
      }
      throw "order does not exist";
    });
};

// funcion para actualizar un Pedido
let updateEstado = async (id, IdEstado, cancela) => {
  // verifico si existe el Pedido
  return (existe = await buscoPedido(id).then((response) => {
    if (response) {
      return pedidoModel.update(
        {
          state_id: IdEstado,
        },
        { where: { id: id } }
      );
    }
  }));
};
// funcion verifica si el pedido creo el mismo usuario o si no se encuentra confirmado
const verificoPedido = async (id) => {
  return await pedidoModel
    .findOne({
      where: {
        [Op.and]: [
          { id: id },
          {
            state_id: { [Op.ne]: 1 },
          },
        ],
      },
    })
    .then((resultado) => {
      if (resultado !== null) {
        throw "ERROR: Order already confirmed";
      } else {
        return pedidoModel
          .findOne({
            where: {
              [Op.and]: [
                { id: id },
                {
                  users_id: { [Op.ne]: userLogin.usuarioLogueado.id },
                },
              ],
            },
          })
          .then((userReturn) => {
            if (userReturn !== null) {
              throw "Error este pedido no te pertenece";
            }
          });
      }
    });
};

// Elimina el pedido
const delet = async (req) => {
  const idOrder = parseInt(req.params.id, 10);
  const result = await pedidoModel.destroy({
    where: { id: idOrder },
  });
  return result;
};

// Actualizacion del precio de los pedidos
let actualizaPecioPedido = async (idPedido) => {
  return await OrdersHasProducts.findAll({
    where: { orders_id: idPedido },
  }).then((resultado) => {
    if (resultado) {
      let total = 0;
      resultado.forEach((element) => {
        total += element.line_price;
      });
      return pedidoModel.update(
        {
          total_pay: total,
        },
        { where: { id: idPedido } }
      );
    }
  });
};

// funcion para agregar un nuevo Pedido
let addProducto = async (nuevoProducto, idPedido, idProducto) => {
  // Verifico si el pedido este en el estado de pendiente y que sea del usuario
  return await verificoPedido(idPedido).then(() => {
    // fecha de hoy en el formato
    let f = new Date();
    // asigno fecha que agrego el producto
    nuevoProducto.fecha = `${f.getDate()}/${f.getMonth()}/${f.getFullYear()}`;
    // asigno hora que agrego el producto
    nuevoProducto.hora = `${f.getHours()}:${f.getMinutes()}`;

    return OrdersHasProducts.findOne({
      where: {
        [Op.and]: [{ products_id: idProducto }, { orders_id: idPedido }],
      },
    }).then((resultado) => {
      if (resultado) {
        throw "the product already exists in the order";
      } else {
        return varProducto
          .buscoProducto(idProducto)
          .then((response) => {
            if (response) {
              let productSelect = response;

              let precio_linea =
                productSelect.price * parseInt(nuevoProducto.cant_pedido, 10);

              //voy a buscar el pedido para ver si existe
              const newProducOrder = OrdersHasProducts.build({
                orders_id: idPedido,
                products_id: idProducto,
                ordered_quantity: parseInt(nuevoProducto.cant_pedido, 10),
                unit_price: productSelect.price,
                line_price: precio_linea,
                observations: nuevoProducto.observaciones,
              });
              const result = newProducOrder.save();
              actualizaPecioPedido(idPedido);
              return result;
            }
          })
          .catch((error) => {
            return error;
          });
      }
    });
  });
};

// funcion para actualizar productos del Pedido
let updateProducto = async (actualizaProducto, idPedido, idProducto) => {
  // Verifico si el pedido este en el estado de pendiente y que sea del usuario
  return await verificoPedido(idPedido).then(() => {
    // fecha de hoy en el formato
    let f = new Date();
    // asigno fecha que agrego el producto
    actualizaProducto.fecha = `${f.getDate()}/${f.getMonth()}/${f.getFullYear()}`;
    // asigno hora que agrego el producto
    actualizaProducto.hora = `${f.getHours()}:${f.getMinutes()}`;
    actualizaProducto.cant_pedido = parseInt(actualizaProducto.cant_pedido, 10);
    //voy a buscar el pedido para ver si existe
    let existe = buscoPedido(idPedido);
    // verifico existe
    return OrdersHasProducts.findOne({
      where: {
        [Op.and]: [{ products_id: idProducto }, { orders_id: idPedido }],
      },
    }).then((resultado) => {
      if (!resultado) {
        throw "the order does not exist";
      } else {
        // dejo la busqueda del producto por que va a traer el precio del producto
        return varProducto
          .buscoProducto(idProducto)
          .then((response) => {
            if (response) {
              let productSelect = response;
              // let precio_unidad = productSelect.price;
              let precio_linea =
                productSelect.price *
                parseInt(actualizaProducto.cant_pedido, 10);
              return OrdersHasProducts.update(
                {
                  orders_id: idPedido,
                  products_id: idProducto,
                  ordered_quantity: parseInt(actualizaProducto.cant_pedido, 10),
                  unit_price: productSelect.price,
                  line_price: precio_linea,
                  observations: actualizaProducto.observaciones,
                },
                {
                  where: {
                    [Op.and]: [
                      { products_id: idProducto },
                      { orders_id: idPedido },
                    ],
                  },
                }
              );
            }
            console.log(`pasa por aca`);
            //  return actualizaPecioPedido(idPedido);
          })
          .catch((error) => {
            return error;
          });
      }
    });
  });
};

module.exports = {
  get,
  getId,
  getUser,
  add,
  addProducto,
  update,
  updateEstado,
  updateProducto,
  delet,
  actualizaPecioPedido,
};
