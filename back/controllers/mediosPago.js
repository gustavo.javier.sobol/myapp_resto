const Sequelize = require("sequelize");
const connection = require("../config/db.config");

const orderModel = require("../models/pedidos");
const medioPagoModel = require("../models/mediosPago");

// funcion devuelve lista con Medios Pagos
let get = async () => {
  try {
    let result = await medioPagoModel.findAll(/* {
    include: [{
      model: orderModel,
      as: "pagos",
    }] 
} */);
    return result;
  } catch (e) {
    console.log(e);
  }
};

// funcion para agregar un nuevo MedioPago
const add = async (req) => {
  console.log(req.body.tipo);
  const newMedioPago = await medioPagoModel.build({
    kind: req.body.tipo,
  });
  const result = await newMedioPago.save();
  return result;
};

// funcion para actualizar un MedioPago
const update = async (req) => {
  const idMedioPago = parseInt(req.params.id, 10);
  return await medioPagoModel
    .findOne({ where: { id: idMedioPago } })
    .then((resultado) => {
      if (resultado) {
        return medioPagoModel.update(
          {
            kind: req.body.tipo,
          },
          { where: { id: idMedioPago } }
        );
      }
      throw "Medio Pago does not exist";
    })
    .catch((error) => {
      return error;
    });
};

// funcion para eliminar un MedioPago
const delet = async (req) => {
  const idMedioPago = parseInt(req.params.id, 10);
  const result = await medioPagoModel.destroy({
    where: { id: idMedioPago },
  });
  return result;
};

module.exports = { get, add, update, delet };
