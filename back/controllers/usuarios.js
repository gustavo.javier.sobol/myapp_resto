require("dotenv").config();
const storage = require("node-sessionstorage");
//const Sequelize = require('sequelize');
const { Op } = require("sequelize");

const connection = require("../config/db.config");
//conecto con el modelo para traer los datos
const userModel = require("../models/users");

const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

// traigo el array de datos de los usuarios
const varUsuario = require("../models/usuario.js");
// funcion devuelve lista con usuarios
let get = async () => await userModel.findAll();

// funcion para agregar un nuevo usuario
const add = async (req) => {
  return await userModel
    .findOne({
      where: {
        [Op.or]: [
          { user: req.body.usuario },
          { mail: req.body.correo_electronico },
        ],
      },
    })
    .then((resul) => {
      // si devuelve un valor true indica que encontro un usuario
      // o correo que ya existe
      if (resul) {
        // entonces disparo un error que emite el mensaje The user or email already exists.
        throw 404;
      }

      let contrasena = bcrypt.hashSync(req.body.contrasena, 10);
      // sino guardo el nuevo usuario y lo retorno
      return userModel
        .build({
          user: req.body.usuario,
          name_lastName: req.body.nombre_y_apellido,
          mail: req.body.correo_electronico,
          telephone: req.body.telefono,
          shipping_address: req.body.direccion_de_envio,
          password: contrasena,
          profile: req.body.perfil,
          discontinued: req.body.suspender,
        })
        .save();
    });
  // si agregro el catch en esta opción no funciona la opción
  // throw declarada anteriormente y no lo puede tomar en mi
  // archivo routes/usuarios.js
  /* .catch(error => {
            return error;
        }) */
};
// funcion para actualizar un usuario
const update = async (req) => {
  const idUser = parseInt(req.params.id, 10);
  return await userModel.findOne({ where: { id: idUser } }).then((user) => {
    if (user) {
      console.log(`usuario: ${req.body.usuario}`);
      let user =
        typeof req.body.usuario === "undefined" ? "" : req.body.usuario;
      let mails =
        typeof req.body.correo_electronico === "undefined"
          ? ""
          : req.body.correo_electronico;
      console.log(`var usuario: ${user}`);
      console.log(`var mails: ${mails}`);
      return userModel
        .findOne({
          where: {
            id: { [Op.notIn]: [idUser] },
            [Op.or]: [{ user: user }, { mail: mails }],
          },
        })
        .then((mail) => {
          if (mail) {
            throw "Existing mails cannot be re-entered. Please enter another";
          } else {
            let contrasena =
              typeof req.body.contrasena === "undefined"
                ? req.body.contrasena
                : bcrypt.hashSync(req.body.contrasena, 10);

            return userModel.update(
              {
                user: req.body.usuario,
                name_lastName: req.body.nombre_y_apellido,
                mail: req.body.correo_electronico,
                telephone: req.body.telefono,
                shipping_address: req.body.direccion_de_envio,
                password: contrasena,
                profile: req.body.perfil,
                discontinued: req.body.suspender,
              },
              { where: { id: idUser } }
            );
          }
        });
    } else {
      throw "User no exists";
    }
  });
};

const delet = async (req) => {
  const idUser = parseInt(req.params.id, 10);
  const result = await userModel
    .destroy({
      where: { id: idUser },
    })
    .then((resul) => {
      if (!resul) {
        throw "User no exists";
      }
    });
  return result;
};

let login = (usuario, contrasena) => {
  // verifico si existe el usuario
  return userModel
    .findOne({
      where: {
        [Op.or]: [{ user: usuario }, { mail: usuario }],
      },
    })
    .then((resultado) => {
      if (resultado) {
        return bcrypt.compare(contrasena, resultado.password).then((resul) => {
          if (resul) {
            if (resultado.discontinued == 0) {
              //  varUsuario.usuarioLogueado = resultado;
              const token = jwt.sign(
                { dataUser: resultado },
                process.env.JWT_SECRET,
                { expiresIn: 3600 }
              );
              //agrego para que lo guarde en una variable global para que no
              // este pidiendo en cada consulta
              varUsuario.token = token;
              return token;
            } else {
              throw "deshabilitado";
            }
          } else {
            return false;
          }
        });
      }
      return false;
    })
    .catch((error) => {
      return error;
    });
};

// funcion devuelve usuario logueado
let getView = (token) => {
  let tokenLLega = token === "undefined" ? varUsuario.token : token;
  // muestro array con el usuario logueado o sino devuelvo un mensaje
  return jwt.decode(tokenLLega, { complete: true });
};

// funcion devuelve usuario logueado
let logout = (req) => {
  // Limpio variable de usuario logueado y token
  req.headers.authorization = "null";
  varUsuario.token = "";
  varUsuario.usuarioLogueado = [];
  storage.removeItem("jwt");
  return `Se cerro correctamente la sesion`;
};

let verifyUser = async (perfilUser) => {
  let verificoUser = await userModel.findOne({
    where: {
      [Op.or]: [
        //  { user: req.body.usuario },
        { mail: perfilUser.mail },
      ],
    },
  });

  // si devuelve un valor true indica que encontro un usuario
  // o correo que ya existe

  if (verificoUser === null) {
    // entonces disparo un error que emite el mensaje The user or email already exists.
    // throw 404;
    let contrasena = await bcrypt.hashSync(perfilUser.mail, 10);
    // sino guardo el nuevo usuario y lo retorno
    const userCreado = userModel.build({
      user: perfilUser.name_lastName,
      name_lastName: perfilUser.name_lastName,
      mail: perfilUser.mail,
      telephone: "123456",
      shipping_address: "Completar Dirección",
      password: contrasena,
      profile: "pedidor",
      discontinued: 0,
      idGoogle: perfilUser.idGoogle,
      idLinkedin: perfilUser.idLinkedin,
    });

    const guardoUser = await userCreado.save();
    console.log(`resul despues del back ${JSON.stringify(userCreado)}`);
    verificoUser = userCreado;
    /*
     */
  } else {
  }
  let token = jwt.sign({ dataUser: verificoUser }, process.env.JWT_SECRET, {
    expiresIn: 3600,
  });
  console.log(`tokeen back ${token}`);
  // esta opcion es la que genera inconvenientes
  // y permite que el token aparesca en todos los navegadores
  // storage.setItem("jwt", token);
  return token;
};

module.exports = {
  verifyUser,
  get,
  add,
  update,
  delet,
  login,
  getView,
  logout,
};
