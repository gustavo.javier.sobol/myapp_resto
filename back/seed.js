const sequelize = require("./config/db.config");
const bcrypt = require("bcrypt");

const User = require("./models/users");
const PaymentMethods = require("./models/mediosPago");
const Product = require("./models/productos");
const Order = require("./models/pedidos");
const OrderHasProduct = require("./models/orders_has_products");

require("./models/asociations");
const users = [];
let hash = bcrypt.hashSync("123456", 10);
users.push({
  user: "admin",
  name_lastName: "admin",
  mail: "admin@admin.com",
  telephone: 123456,
  shipping_address: "rivadavia 557",
  password: hash,
  profile: "admin",
});
users.push({
  user: "user",
  name_lastName: "user",
  mail: "user@user.com",
  telephone: 123456,
  shipping_address: "rivadavia 557",
  password: hash,
  profile: "pedidor",
});

//Medios de pagos
const MediosDePagos = [{ kind: "Contado" }, { kind: "Tarjeta" }];

// Products
const products = [
  { description: "Hamburguesa", price: 5 },
  { description: "Papas Fritas", price: 2 },
  { description: "Pizza", price: 8 },
  { description: "Empanada", price: 1 },
];
// Orders
const orders = [
  {
    state_id: "1",
    application_date: "2021-01-01",
    request_time: "08:00",
    shipping_address: "Avenida siempre viva 742",
    total_pay: 5,
    users_id: 2,
    payment_methods_id: 1,
  },
  {
    state_id: "1",
    application_date: "2021-01-01",
    request_time: "08:00",
    shipping_address: "Avenida siempre viva 732",
    total_pay: 3,
    users_id: 1,
    payment_methods_id: 1,
  },
];
// Orders Products
const ordersHasProducts = [
  {
    orders_id: 1,
    products_id: 1,
    ordered_quantity: 2,
    unit_price: 500,
    observations: "",
    line_price: 0,
  },
  {
    orders_id: 1,
    products_id: 2,
    ordered_quantity: 1,
    unit_price: 200,
    observations: "",
    line_price: 0,
  },
  {
    orders_id: 2,
    products_id: 3,
    ordered_quantity: 2,
    unit_price: 500,
    observations: "",
    line_price: 0,
  },
  {
    orders_id: 1,
    products_id: 4,
    ordered_quantity: 1,
    unit_price: 200,
    observations: "",
    line_price: 0,
  },
];

//{force: true,alter: true }

sequelize
  .sync({ force: true })
  .then(() => {
    // Conexión establecida
    console.log("Conexión establecida...");
  })
  .then(() => {
    // Rellenar usuarios
    users.forEach((user) => User.create(user));
  })
  .then(() => {
    // Rellenar usuarios
    MediosDePagos.forEach((pago) => PaymentMethods.create(pago));
  })
  .then(() => {
    // Rellenar direcciones
    products.forEach((product) => Product.create(product));
  })
  .then(() => {
    // Rellenar posts
    orders.forEach((order) => Order.create(order));
  })
  .then(() => {
    // Rellenar posts
    ordersHasProducts.forEach((orderProduct) =>
      OrderHasProduct.create(orderProduct)
    );
  });
