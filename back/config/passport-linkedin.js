const passport = require("passport");
const LinkedInStrategy = require("passport-linkedin-oauth2").Strategy;
const usuarios = require("../controllers/usuarios.js");
const storage = require("node-sessionstorage");
passport.serializeUser(function (user, cb) {
  cb(null, user);
});

passport.deserializeUser(function (obj, cb) {
  cb(null, obj);
});

passport.use(
  new LinkedInStrategy(
    {
      clientID: process.env.LINKEDIN_CLIENT_ID,
      clientSecret: process.env.LINKEDIN_CLIENT_SECRET,
      callbackURL: `https://${process.env.HOST}/api/linkedin/callback`,
      scope: ["r_emailaddress", "r_liteprofile"],
    },
    (accessToken, refreshToken, profile, done) => {
      let perfilUser = {
        idLinkedin: profile.id,
        mail: profile.emails[0]["value"],
        name_lastName: profile.displayName,
      };

      usuarios
        .verifyUser(perfilUser)
        .then((result) => {
          storage.setItem("jwt", result);
          done(null, result);
        })
        .catch((error) => {
          console.log(`error ${error}`);
          return false;
        });
      /*  process.nextTick(() => {
        return done(null, profile);
      }); */
    }
  )
);
