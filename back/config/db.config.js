require('dotenv').config()
const { Sequelize } = require('sequelize');

const sequelize = new Sequelize(process.env.MYSQL_DB_NAME, process.env.MYSQL_USER, process.env.MYSQL_PASS, {
  host: process.env.MYSQL_HOST,
  port: process.env.MYSQL_PORT,
  dialect: 'mysql',
  define: {
    freezeTableName: true
  }
});
//Se debe ejecutar cuando inicializa el programa
// sequelize.sync({force: true, alter: true }).then(() => "Database created");
// force: true,
module.exports = sequelize;