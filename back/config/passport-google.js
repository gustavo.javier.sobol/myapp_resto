require("dotenv").config();

const usuarios = require("../controllers/usuarios.js");
const passport = require("passport");
var GoogleStrategy = require("passport-google-oauth20");
const storage = require("node-sessionstorage");

//passport.use(cors());
passport.use(
  new GoogleStrategy(
    {
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      callbackURL: `https://${process.env.HOST}/api/callback`,
    },
    function (request, accessToken, refreshToken, profile, done) {
      console.log(`user linke ${JSON.stringify(profile)}`);
      let perfilUser = {
        idGoogle: profile.id,
        mail: profile.emails[0]["value"],
        name_lastName: profile.displayName,
      };

      usuarios
        .verifyUser(perfilUser)
        .then((result) => {
          storage.setItem("jwt", result);
          done(null, result);
        })
        .catch((error) => {
          console.log(`error ${error}`);
          return false;
        });
    }
  )
);
