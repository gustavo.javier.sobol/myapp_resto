require("dotenv").config();
const express = require("express");
const router = express.Router();
const cors = require("cors");
const passport = require("passport");
const storage = require("node-sessionstorage");
router.use(cors());
// sirve para recibir req
router.use(express.urlencoded({ extended: true }));
// traigo el array de datos de los usuarios
const usuarios = require("../controllers/usuarios.js");

const midUsers = require("../middlewares/usuarios");
const { token } = require("../models/usuario.js");
router.get("/checkLogin", midUsers.userLog, function (req, res) {
  return res.status(200);
});

/**
 * @swagger
 * tags:
 *   name: Login
 *   description: Acciones de login y creación de usuarios.
 * paths:
 *   /api/login:
 *    post:
 *      tags: [Login]
 *      description: Login de Usuario
 *      summary: "Ingreso al sistema "
 *      parameters:
 *       - name: usuario
 *         description: Nombre del usuario
 *         in: formData
 *         required: true
 *         type: string
 *       - name: contrasena
 *         description: Contraseña del usuario
 *         in: formData
 *         required: true
 *         type: string
 *         format: password
 *      responses:
 *        200:
 *          description: Success
 */
router.post("/login", function (req, res) {
  let resLogin = usuarios.login(req.body.usuario, req.body.contrasena);

  resLogin
    .then((result) => {
      if (result) {
        let mensaje = result;
        if (result === "deshabilitado") {
          mensaje = "Tu usuario se encuentra deshabilitado";
          res.status(401).send({
            status: 401,
            message: mensaje,
          });
        } else {
          res.status(200).send({
            status: 200,
            message: mensaje,
          });
        }
      } else {
        res.status(400).send({
          status: 400,
          message: `Wrong username or password.`,
          // errors: `${error}`,
        });
      }
    })
    .catch((error) => {
      res.send(400).send({
        message: `Unable to Update data. `,
        errors: `${error}`,
        status: 400,
      });
    });
});
/**
 * @swagger
 * paths:
 *   /api/view:
 *    get:
 *      tags: [Login]
 *      description: Muestra el usuario actual
 *      summary: "Muestra el usuario activo"
 *      security:
 *        - jwt: []
 *      responses:
 *        200:
 *          description: Success
 */

router.get("/view", function (req, res) {
  let user = usuarios.getView(req.headers.authorization);
  res.json(user);
});
/**
 * @swagger
 * paths:
 *   /api/logout:
 *    delete:
 *      tags: [Login]
 *      description: Saca al usuario actual
 *      summary: "Desloguea el usuario"
 *      responses:
 *        200:
 *          description: Success
 */
router.delete("/logout", function (req, res) {
  res.json(usuarios.logout(req));
});

/**
 * @swagger
 * paths:
 *   /api/nuevo:
 *    post:
 *      tags: [Login]
 *      description: Crea un nuevo usuario
 *      summary: "Permite al usuario crear su usuario personal"
 *      parameters:
 *       - name: usuario
 *         description: Nombre del usuario
 *         in: formData
 *         required: true
 *         type: string
 *       - name: nombre_y_apellido
 *         description: Nombre y Apellido del usuario
 *         in: formData
 *         required: true
 *         type: string
 *       - name: correo_electronico
 *         description: Correo electronico del usuario
 *         in: formData
 *         required: true
 *         type: string
 *       - name: telefono
 *         description: Número de telefono del usuario
 *         in: formData
 *         required: true
 *         type: integer
 *       - name: direccion_de_envio
 *         description: Direccion de envio predeterminado del usuario
 *         in: formData
 *         required: true
 *         type: string
 *       - name: contrasena
 *         description: Contraseña del usuario
 *         in: formData
 *         required: true
 *         type: string
 *         format: password
 *      responses:
 *        200:
 *          description: Success
 *
 */
router.post("/nuevo", function (req, res) {
  /* let nuevoUsuario =  req.body;
  nuevoUsuario.perfil = "pedidor";
 */
  // res.json({ msj: usuarios.add(nuevoUsuario) });
  req.body.perfil = "pedidor";
  usuarios
    .add(req)
    .then((response) => {
      if (response) {
        res.status(200).send({
          status: 200,
          message: `Data Save Successfully.`,
        });
      }
    })
    .catch((error) => {
      if (error == 404) {
        res.status(404).send({
          status: 404,
          message: `The user or email already exists.`,
          errors: `${error}`,
        });
      } else {
        res.status(400).send({
          message: "Unable to insert data",
          errors: error,
          status: 400,
        });
      }
    });
});
/**
 * @swagger
 * tags:
 *   name: google
 *   description: Acciones de login desde google.
 * paths:
 *   /api/google:
 *    get:
 *      tags: [google]
 *      description: google de Usuario
 *      summary: "Ingreso al sistema desde google"
 *      responses:
 *        200:
 *          description: Success
 */
router.get(
  "/google",
  passport.authenticate("google", {
    scope: ["profile", "email"],
  }),

  function (req, res) {}
);
router.get(
  "/callback",
  passport.authenticate("google", {
    successRedirect: `https://${process.env.HOST_FRONT}/#/orders`,
    failureRdirect: `https://${process.env.HOST_FRONT}/#/`,
    session: false,
  }),
  midUsers.postCallBack,
  function (req, res) {}
);
router.get("/returnSession", function (req, res) {
  if (storage.getItem("jwt") !== undefined) {
    tokenDevuelto = storage.getItem("jwt");
    storage.removeItem("jwt");
    res.status(200).send({
      status: 200,
      message: tokenDevuelto,
    });
  } else {
    res.status(401).send({
      status: 401,
      message: "null",
    });
  }
});

router.get(
  "/linkedin",
  passport.authenticate("linkedin", {
    scope: ["r_emailaddress", "r_liteprofile"],
  })
);
router.get(
  "/linkedin/callback",
  passport.authenticate("linkedin", {
    successRedirect: `https://${process.env.HOST_FRONT}/#/orders`,
    failureRedirect: `https://${process.env.HOST_FRONT}/#/`,
  })
);

module.exports = router;
