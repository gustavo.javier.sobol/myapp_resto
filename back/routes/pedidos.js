require("dotenv").config();
const express = require("express");
const router = express.Router();
// sirve para recibir req
router.use(express.urlencoded({ extended: true }));
// traigo el array de datos de los pedidos
const pedidos = require("../controllers/pedidos.js");
// importacion de middlewes
const midUsers = require("../middlewares/usuarios");
let idPedido = 0;
const storage = require("node-sessionstorage");

// Paypal
const request = require("request");
// router.use(express.urlencoded({ extended: true }));
/**
 * 1️⃣ Paso
 * Crear una aplicacion en Ppaypal
 * Aqui agregamos las credenciales de nuestra app de PAYPAL
 * https://developer.paypal.com/developer/applications (Debemos acceder con nuestra cuenta de Paypal)
 * [Cuentas de TEST] https://developer.paypal.com/developer/accounts/
 */

const CLIENT = process.env.PAYPAL_CLIENT_ID;
const SECRET = process.env.PAYPAL_CLIENT_SECRET;
const PAYPAL_API = "https://api-m.sandbox.paypal.com"; // Live https://api-m.paypal.com

const auth = { user: CLIENT, pass: SECRET };

/**
 * Establecemos los contraladores que vamos a usar
 */

const createPayment = (req, res) => {
  storage.setItem("jwt", req.headers.authorization);
  idPedido = req.body.id;
  const body = {
    intent: "CAPTURE",
    purchase_units: [
      {
        amount: {
          currency_code: "USD", //https://developer.paypal.com/docs/api/reference/currency-codes/
          value: req.body.precioPagar,
        },
      },
    ],
    application_context: {
      brand_name: `MiTienda.com`,
      landing_page: "NO_PREFERENCE", // Default, para mas informacion https://developer.paypal.com/docs/api/orders/v2/#definition-order_application_context
      user_action: "PAY_NOW", // Accion para que en paypal muestre el monto del pago
      return_url: `https://${process.env.HOST}/api/pedidos/execute-payment`, // Url despues de realizar el pago
      cancel_url: `https://${process.env.HOST}/api/pedidos/cancel-payment`, // Url despues de realizar el pago
    },
  };
  //https://api-m.sandbox.paypal.com/v2/checkout/orders [POST]

  request.post(
    `${PAYPAL_API}/v2/checkout/orders`,
    {
      auth,
      body,
      json: true,
    },
    (err, response) => {
      if (res.statusCode == 200) {
        res.send(response.body);
      } else {
        console.log(`not found `);
      }
    }
  );
};

/**
 * Esta funcion captura el dinero REALMENTE
 * @param {*} req
 * @param {*} res
 */
const executePayment = (req, res) => {
  storage.removeItem("jwt");
  const token = req.query.token; //<-----------
  request.post(
    `${PAYPAL_API}/v2/checkout/orders/${token}/capture`,
    {
      auth,
      body: {},
      json: true,
    },
    (err, response) => {
      try {
        pedidos
          .updateEstado(idPedido, 2)
          .then((result) => {})
          .catch((error) => {
            res.status(400).send({
              message: "Unable to find data",
              errors: error,
              status: 400,
            });
          });
        res.redirect(
          `https://${process.env.HOST_FRONT}:/#/orderGenerate?estado=${response.body.status}&id=`
        );
        //  res.send({ data: response.body });
      } catch (error) {}
    }
  );
};

const cancelPayment = (req, res) => {
  storage.removeItem("jwt");
  // return to home from
  res.redirect(
    `https://${process.env.HOST_FRONT}/#/orderGenerate?estado=Cancelado`
  );
};
/**
 * 2️⃣ Creamos Ruta para generar pagina de CHECKOUT -
 */

//    https://localhost:3000/create-payment [POST]
router.post(`/create-payment`, createPayment);

/**
 * 3️⃣ Creamos Ruta para luego que el cliente completa el checkout
 * debemos de capturar el dinero!
 */
router.get(`/execute-payment`, executePayment);

/**
 * 4 Creamos Ruta por si el cliente completa el cancelar
 * debemos de capturar el cancel!
 */
router.get(`/cancel-payment`, cancelPayment);

// end paypal
/**
 * @swagger
 * tags:
 *   name: Pedidos
 *   description: CRUD de Pedidos.
 * paths:
 *   /api/pedidos/todos/:
 *    get:
 *      description: Lista todos los pedidos
 *      summary: "SEQUELIZE Lista todos los pedidos (Solo Administradores)"
 *      tags: [Pedidos]
 *      security:
 *        - jwt: []
 *      responses:
 *        200:
 *          description: Success
 */
router.get("/todos/", midUsers.userAdmin, function (req, res) {
  /*   res.send(pedidos.get());
   */
  pedidos
    .get()
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data find Successfully",
        data: result,
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to find data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * paths:
 *   /api/pedidos/{id}/{idEstado}:
 *    put:
 *      tags: [Pedidos]
 *      description: Actualiza el estado de un pedido
 *      summary: "SEQUELIZE  Actualiza el estado de un pedido (Solo Adminitrador) "
 *      parameters:
 *       - name: id
 *         description: Id Pedido a cambiar el estado
 *         in: path
 *         required: true
 *         type: integer
 *       - name: idEstado
 *         description: Nuevo estado del pedido
 *         in: path
 *         required: false
 *         type: string
 *      security:
 *        - jwt: []
 *      responses:
 *        200:
 *          description: Success
 */
router.put("/:id/:idEstado", midUsers.userAdmin, function (req, res) {
  let id = parseInt(req.params.id, 10);
  let idEstado = parseInt(req.params.idEstado, 10);
  // res.send({msj: pedidos.updateEstado(id, idEstado)});
  pedidos
    .updateEstado(id, idEstado)
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data find Successfully",
        data: result,
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to find data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * paths:
 *   /api/pedidos:
 *    get:
 *      description: Lista todos los pedidos
 *      summary: "SEQUELIZE Lista todos los pedidos de un usuario"
 *      tags: [Pedidos]
 *      security:
 *        - jwt: []
 *      responses:
 *        200:
 *          description: Success
 */
router.get("/", function (req, res) {
  /* res.send(pedidos.getUser()); */
  pedidos
    .getUser()
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data find Successfully",
        data: result,
      });
    })
    .catch((error) => {
      res.send({
        message: "Unable to find data",
        errors: error,
        status: 400,
      });
    });
});
/**
 * @swagger
 * paths:
 *   /api/pedidos/{id}:
 *    get:
 *      description:  Muestra el detalle del pedido
 *      summary: "SEQUELIZE Muestra el detalle del pedido (solo administrador)"
 *      parameters:
 *       - name: id
 *         description: Id del pedido a buscar
 *         summary: "Devuelve el detalle del pedido"
 *         in: path
 *         required: true
 *         type: integer
 *      tags: [Pedidos]
 *      security:
 *        - jwt: []
 *      responses:
 *        200:
 *          description: Success
 */
router.get("/:id", midUsers.userAdmin, function (req, res) {
  let id = parseInt(req.params.id, 10);
  /*   res.send(pedidos.getId(id));
   */
  pedidos
    .getId(id)
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data find Successfully",
        data: result,
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to find data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * paths:
 *   /api/pedidos:
 *    post:
 *      tags: [Pedidos]
 *      description:  Crea un nuevo pedido, el usuario va a tomar por determinado el que se encuentra logueado
 *      summary: " SEQUELIZE Crea un nuevo pedido"
 *      parameters:
 *       - name: direccion_envio
 *         description: Dirección de envio del pedido. Si deja el campo en blanco va a tomar la dirección predetermiada del usuario
 *         in: formData
 *         required: false
 *         type: string
 *       - name: medio_pago
 *         description: Debe indicar el medio de pago
 *         in: formData
 *         required: false
 *         type: string
 *      security:
 *        - jwt: []
 *      responses:
 *        200:
 *          description: Success
 *
 */
router.post("/", function (req, res) {
  // res.send({ msj: pedidos.add(req) });
  pedidos
    .add(req)
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: `Data Save Successfully`,
        id: result.id,
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to insert data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * paths:
 *   /api/pedidos/{id}:
 *    put:
 *      tags: [Pedidos]
 *      description:  Actualizar el pedido
 *      summary: " SEQUELIZE Actualiza el pedido"
 *      parameters:
 *       - name: id
 *         description: Id del pedido a actualizar
 *         in: path
 *         required: true
 *         type: integer
 *       - name: direccion_envio
 *         description: Dirección de envio del pedido. Si deja el campo en blanco va a tomar la dirección predetermiada del usuario
 *         in: formData
 *         required: false
 *         type: string
 *       - name: medio_pago
 *         description: Debe indicar el medio de pago
 *         in: formData
 *         required: false
 *         type: string
 *      security:
 *        - jwt: []
 *      responses:
 *        200:
 *          description: Success
 */
router.put("/:id", midUsers.userAdmin, function (req, res) {
  /* let id =  parseInt(req.params.id, 10);
    res.send({msj: pedidos.update(req.body, id)});
 */
  pedidos
    .update(req)
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data Update Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: `Unable to Update data. `,
        errors: `${error}`,
        status: 400,
      });
    });
});

/**
 * @swagger
 * paths:
 *   /api/pedidos/{idPedido}/productos/{idProducto}:
 *    post:
 *      tags: [Pedidos]
 *      description: Ingresa un producto nuevo al pedido
 *      summary: "Ingresa un producto nuevo al pedido"
 *      parameters:
 *       - name: idPedido
 *         description: Id del pedido donde ingresar el producto
 *         in: path
 *         required: true
 *         type: integer
 *       - name: idProducto
 *         description: Id del producto a ingresar
 *         in: path
 *         required: true
 *         type: integer
 *       - name: cant_pedido
 *         description: Cantidad pedida
 *         in: formData
 *         required: true
 *         type: integer
 *       - name: observaciones
 *         description: Alguna observacion que quiera dar sobre el producto
 *         in: formData
 *         required: false
 *         type: string
 *      security:
 *        - jwt: []
 *      responses:
 *        200:
 *          description: Success
 */
router.post("/:idPedido/productos/:idProducto", function (req, res) {
  let idPedido = parseInt(req.params.idPedido, 10);
  let idProducto = parseInt(req.params.idProducto, 10);

  pedidos
    .addProducto(req.body, idPedido, idProducto)
    .then((result) => {
      pedidos.actualizaPecioPedido(idPedido);
      res.status(200).send({
        status: 200,
        message: "Data Update Successfully",
        data: result,
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: `Unable to Update data. `,
        errors: `${error}`,
        status: 400,
      });
    });
});

/**
 * @swagger
 * paths:
 *   /api/pedidos/{idPedido}/productos/{idProducto}:
 *    put:
 *      tags: [Pedidos]
 *      description: Actualiza un producto nuevo al pedido
 *      summary: "Actualiza un producto nuevo al pedido"
 *      parameters:
 *       - name: idPedido
 *         description: Id del pedido donde ingresar el producto
 *         in: path
 *         required: true
 *         type: integer
 *       - name: idProducto
 *         description: Id del producto a ingresar
 *         in: path
 *         required: true
 *         type: integer
 *       - name: cant_pedido
 *         description: Cantidad pedida
 *         in: formData
 *         required: true
 *         type: integer
 *       - name: observaciones
 *         description: Alguna observación que quiera dar sobre el producto
 *         in: formData
 *         required: false
 *         type: string
 *      security:
 *        - jwt: []
 *      responses:
 *        200:
 *          description: Success
 */
router.put("/:idPedido/productos/:idProducto", function (req, res) {
  let idPedido = parseInt(req.params.idPedido, 10);
  let idProducto = parseInt(req.params.idProducto, 10);
  // res.send({msj: pedidos.updateProducto(req.body, idPedido, idProducto )});
  pedidos
    .updateProducto(req.body, idPedido, idProducto)
    .then((result) => {
      pedidos.actualizaPecioPedido(idPedido);
      res.status(200).send({
        status: 200,
        message: "Data Update Successfully",
        data: result,
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: `Unable to Update data. `,
        errors: `${error}`,
        status: 400,
      });
    });
});

/**
 * @swagger
 * paths:
 *   /api/pedidos/{id}/confirma:
 *    post:
 *      tags: [Pedidos]
 *      description: Actualiza el estado a confirmado
 *      summary: "SEQUELIZE Actualiza el estado a confirmado "
 *      parameters:
 *       - name: id
 *         description: Id Pedido a cambiar el estado
 *         in: path
 *         required: true
 *         type: integer
 *      security:
 *        - jwt: []
 *      responses:
 *        200:
 *          description: Success
 */
router.post("/:id/confirma", function (req, res) {
  let id = parseInt(req.params.id, 10);
  // Estado 2 confirmado
  let idEstado = parseInt(2, 10);
  // res.send({msj: pedidos.updateEstado(id, idEstado)});
  pedidos
    .updateEstado(id, idEstado)
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data find Successfully",
        data: result,
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to find data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * paths:
 *   /api/pedidos/{id}/cancela:
 *    post:
 *      tags: [Pedidos]
 *      description: Actualiza el estado a cancelado
 *      summary: "SEQUELIZE Actualiza el estado a cancelado "
 *      parameters:
 *       - name: id
 *         description: Id Pedido a cambiar el estado
 *         in: path
 *         required: true
 *         type: integer
 *      security:
 *        - jwt: []
 *      responses:
 *        200:
 *          description: Success
 */
router.post("/:id/cancela", function (req, res) {
  let id = parseInt(req.params.id, 10);
  let idEstado = parseInt(6, 10);

  // res.send({msj: pedidos.updateEstado(id, idEstado, true)});
  pedidos
    .updateEstado(id, idEstado, true)
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data find Successfully",
        data: result,
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to find data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * paths:
 *   /api/pedidos/{id}:
 *    delete:
 *      tags: [Pedidos]
 *      description: Elimina un pedido de acuerdo a su id
 *      summary: " SEQUELIZE Elimina un pedido de acuerdo a su id (solo administrador)"
 *      parameters:
 *      - name: id
 *        description: Id del pedido que desea eliminar
 *        in: path
 *        type: integer
 *      security:
 *        - jwt: []
 *      responses:
 *        200:
 *          description: Success
 */
router.delete("/:id", midUsers.userAdmin, function (req, res) {
  /*   let id =  parseInt(req.params.id, 10);
    res.send(pedidos.delet(id)); */

  pedidos
    .delet(req)
    .then(() => {
      res.status(200).send({
        status: 200,
        message: "Data Delete Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to Delete data",
        errors: error,
        status: 400,
      });
    });
});
/**
 * @swagger
 * definitions:
 *   Pedidos:
 *     type: "object"
 *     properties:
 *       id:
 *         type: "integer"
 *         format: "int64"
 *       estado:
 *         type: "integer"
 *         format: "int64"
 *         $ref: "#/definitions/Estados"
 *       fecha:
 *         type: "string"
 *         format: "date"
 *       hora:
 *         type: "string"
 *         format: "hour"
 *       usuario:
 *         type: "string"
 *         $ref: "#/definitions/Usuarios"
 *       direccion_envio:
 *         type: "string"
 *       medio_pago:
 *         type: "integer"
 *         format: "int64"
 *         $ref: "#/definitions/MediosPago"
 *       total_pagar:
 *         type: "integer"
 *         format: "int64"
 *       productos:
 *         $ref: "#/definitions/ListaProductos"
 *   ListaProductos:
 *     type: "object"
 *     properties:
 *       id_producto:
 *         type: "integer"
 *         format: "int64"
 *         $ref: "#/definitions/Productos"
 *       cant_pedido:
 *         type: "integer"
 *         format: "int64"
 *       precio_unidad:
 *         type: "integer"
 *         format: "int64"
 *       observaciones:
 *         type: "string"
 *       fecha:
 *         type: "string"
 *         format: "date"
 *       hora:
 *         type: "string"
 *         format: "hour"
 *   Estados:
 *     type: "object"
 *     properties:
 *       id:
 *         type: "integer"
 *         format: "int64"
 *       estado:
 *         type: "string"
 *       detalle:
 *         type: "string"
 */
module.exports = router;
