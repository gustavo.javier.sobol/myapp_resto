const express = require("express");
const router = express.Router();
// sirve para recibir req
router.use(express.urlencoded({ extended: true }));
// traigo el array de datos de los usuarios
const usuarios = require("../controllers/usuarios.js");

/**
 * @swagger
 * tags:
 *   name: Usuario
 *   description: CRUD de Usuarios (Solo Administradores).
 * paths:
 *   /api/usuarios:
 *    get:
 *      tags: [Usuario]
 *      description: lista todos los usuarios
 *      summary: "Muestra todos los usuarios"
 *      security:
 *        - jwt: []
 *      responses:
 *        200:
 *          description: Success
 */
router.get("/", function (req, res) {
  usuarios
    .get()
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data find Successfully",
        data: result,
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to find data",
        errors: error,
        status: 400,
      });
    });
});
/**
 * @swagger
 * paths:
 *   /api/usuarios:
 *    post:
 *      tags: [Usuario]
 *      description: Crea un nuevo usuario
 *      summary: "Creación de un usuario"
 *      parameters:
 *       - name: usuario
 *         description: Nombre del usuario
 *         in: formData
 *         required: true
 *         type: string
 *       - name: nombre_y_apellido
 *         description: Nombre y Apellido del usuario
 *         in: formData
 *         required: true
 *         type: string
 *       - name: correo_electronico
 *         description: Correo electronico del usuario
 *         in: formData
 *         required: true
 *         type: string
 *       - name: telefono
 *         description: Número de telefono del usuario
 *         in: formData
 *         required: true
 *         type: integer
 *       - name: direccion_de_envio
 *         description: Dirección de envio predeterminado del usuario
 *         in: formData
 *         required: true
 *         type: string
 *       - name: contrasena
 *         description: Contraseña del usuario
 *         in: formData
 *         required: true
 *         type: string
 *       - name: perfil
 *         description: Perfil de los usuario dependiendo del perfil puede realizar diferentes acciones los disponibles son admin o pedidor
 *         in: formData
 *         required: true
 *         type: string
 *       - name: suspender
 *         description: Suspender usuario
 *         in: formData
 *         required: false
 *         type: boolean
 *         default: false
 *      security:
 *        - jwt: []
 *      responses:
 *        200:
 *          description: Success
 *
 */
router.post("/", function (req, res) {
  usuarios
    .add(req)
    .then((response) => {
      if (response) {
        console.log(response);
        res.status(200).send({
          status: 200,
          message: `Data Save Successfully.`,
        });
      }
    })
    .catch((error) => {
      console.log(error);
      if (error == 404) {
        res.status(404).send({
          status: 404,
          message: `The user or email already exists.`,
          errors: `${error}`,
        });
      } else {
        res.status(400).send({
          message: "Unable to insert data",
          errors: error,
          status: 400,
        });
      }
    });
});
/**
 * @swagger
 * paths:
 *   /api/usuarios/{id}:
 *    put:
 *      tags: [Usuario]
 *      description: Actualiza un usuario
 *      summary: "Actualiza un usuario"
 *      parameters:
 *       - name: id
 *         description: Id del usuario a buscar
 *         in: path
 *         required: true
 *         type: integer
 *       - name: usuario
 *         description: Nuevo nombre de usuario (en el caso que no lo desee modificar deje el campo en blanco)
 *         in: formData
 *         required: false
 *         type: string
 *       - name: nombre_y_apellido
 *         description: Nombre y Apellido del usuario
 *         in: formData
 *         required: false
 *         type: string
 *       - name: correo_electronico
 *         description: Correo electronico del usuario
 *         in: formData
 *         required: false
 *         type: string
 *       - name: telefono
 *         description: Número de telefono del usuario
 *         in: formData
 *         required: false
 *         type: integer
 *       - name: direccion_de_envio
 *         description: Direccion de envio predeterminado del usuario
 *         in: formData
 *         required: false
 *         type: string
 *       - name: contrasena
 *         description: Contraseña del usuario
 *         in: formData
 *         required: false
 *         type: string
 *       - name: perfil
 *         description: Perfil de los usuario dependiendo del perfil puede realizar diferentes acciones los disponibles son admin o pedidor
 *         in: formData
 *         required: false
 *         type: string
 *       - name: suspender
 *         description: Suspender usuario
 *         in: formData
 *         required: false
 *         type: boolean
 *         default: false
 *      security:
 *        - jwt: []
 *      responses:
 *        200:
 *          description: Success
 */
router.put("/:id", function (req, res) {
  /*     let nombreUser =  req.params.nombreUsuario;
      res.json({msj: usuarios.update(req.body, nombreUser)}); */
  const idUser = parseInt(req.params.id, 10);
  console.log(`id: llega ${idUser}`);
  usuarios
    .update(req)
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data Update Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: `Unable to Update data. `,
        errors: `${error}`,
        status: 400,
      });
    });
});
/**
 * @swagger
 * paths:
 *   /api/usuarios/{id}:
 *    delete:
 *      tags: [Usuario]
 *      description: Elimina un usuario
 *      summary: "Eliminación de un usuario"
 *      parameters:
 *      - name: id
 *        description: id del Usuario a eliminar
 *        in: path
 *        type: integer
 *        required: true
 *      security:
 *        - jwt: []
 *      responses:
 *        200:
 *          description: Success
 */
router.delete("/:id", function (req, res) {
  usuarios
    .delet(req)
    .then(() => {
      res.status(200).send({
        status: 200,
        message: "Data Delete Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to Delete data",
        errors: error,
        status: 400,
      });
    });
});
/**
 * @swagger
 * definitions:
 *   Usuarios:
 *     type: "object"
 *     properties:
 *       usuario:
 *         type: "string"
 *       nombre_y_apellido:
 *         type: "string"
 *       correo_electronico:
 *         type: "string"
 *       telefono:
 *         type: "integer"
 *         format: "int64"
 *       direccion_de_envio:
 *         type: "string"
 *       contrasena:
 *         type: "integer"
 *       perfil:
 *         type: "string"
 *       suspender:
 *         type: boolean
 */
module.exports = router;
