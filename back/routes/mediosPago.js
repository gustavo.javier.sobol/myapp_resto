const express = require("express");
const router = express.Router();
// sirve para recibir req
router.use(express.urlencoded({ extended: true }));
// traigo el array de datos de los mediosPagos
const mediosPagos = require("../controllers/mediosPago.js");
const midUsers = require("../middlewares/usuarios");

/**
 * @swagger
 * tags:
 *   name: Medios_de_Pagos
 *   description: CRUD de medios de pagos (Solo Administradores).
 * paths:
 *   /api/mediosPago:
 *    get:
 *      tags: [Medios_de_Pagos]
 *      summary: "Devuelte todos los medios de pagos"
 *      description: lista todos los Medios de Pagos
 *      security:
 *        - jwt: []
 *      responses:
 *        200:
 *          description: Success
 */
router.get("/", function (req, res) {
  // res.json(mediosPagos.get());
  mediosPagos
    .get()
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data find Successfully",
        data: result,
      });
    })
    .catch((error) => {
      /*       res.status(400).send({
        message: "Unable to find data",
        errors: error,
        status: 400,
      }); */
    });
});
/**
 * @swagger
 * paths:
 *   /api/mediosPago:
 *    post:
 *      tags: [Medios_de_Pagos]
 *      description: Crea un nuevo medio de pago
 *      summary: "Permite agregar un nuevo medio de pago"
 *      parameters:
 *       - name: tipo
 *         description: Descripción del medio pago que desea dar de alta
 *         in: formData
 *         required: true
 *         type: string
 *      security:
 *        - jwt: []
 *      responses:
 *        200:
 *          description: Success
 *
 */
router.post("/", midUsers.userAdmin, function (req, res) {
  /* res.json({ msj: mediosPagos.add(req.body) });
   */
  mediosPagos
    .add(req)
    .then(() => {
      res.status(200).send({
        status: 200,
        message: "Data Save Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to insert data",
        errors: error,
        status: 400,
      });
    });
});
/**
 * @swagger
 * paths:
 *   /api/mediosPago/{id}:
 *    put:
 *      tags: [Medios_de_Pagos]
 *      description: Actualiza un medio de pago
 *      summary: "Actualiza un medio de pago"
 *      parameters:
 *       - name: id
 *         description: id del medio de pago que desea actualizar
 *         in: path
 *         required: true
 *         type: integer
 *       - name: tipo
 *         description: Descripción medio pago que desea modificar
 *         in: formData
 *         required: true
 *         type: string
 *      security:
 *        - jwt: []
 *      responses:
 *        200:
 *          description: Success
 */
router.put("/:id", midUsers.userAdmin, function (req, res) {
  /*     let id =  parseInt(req.params.id, 10);
    res.json({msj: mediosPagos.update(req.body, id)});
 */
  mediosPagos
    .update(req)
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data Update Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: `Unable to Update data. `,
        errors: `${error}`,
        status: 400,
      });
    });
});
/**
 * @swagger
 * paths:
 *   /api/mediosPago/{id}:
 *    delete:
 *      tags: [Medios_de_Pagos]
 *      description: Elimina un medio de pago de acuerdo a su id
 *      summary: "Elimina un medio de pago según su id"
 *      parameters:
 *      - name: id
 *        description: Id del medio de pago que desea eliminar
 *        in: path
 *        type: integer
 *      security:
 *        - jwt: []
 *      responses:
 *        200:
 *          description: Success
 */
router.delete("/:id", midUsers.userAdmin, function (req, res) {
  /*     let id =  parseInt(req.params.id, 10);
    res.json(mediosPagos.delet(id)); */
  mediosPagos
    .delet(req)
    .then(() => {
      res.status(200).send({
        status: 200,
        message: "Data Delete Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to Delete data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * definitions:
 *   MediosPago:
 *     type: "object"
 *     properties:
 *       id:
 *         type: "integer"
 *         format: "int64"
 *       tipo:
 *         type: "string"
 */

module.exports = router;
