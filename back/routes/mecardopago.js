require("dotenv").config();
const express = require("express");
const router = express.Router();
const storage = require("node-sessionstorage");
const pedidos = require("../controllers/pedidos.js");

let idPedido = 0;
// Mercado Pago

// SDK de Mercado Pago
const mercadopago = require("mercadopago");

// Agrega credenciales
mercadopago.configure({
  access_token: process.env.MERCADO_ACCESS_TOKEN,
});

router.get("/mpago", (req, res) => {
  tokenRecibido = req.query.token;
  req.headers.authorization = tokenRecibido;
  storage.setItem("jwt", tokenRecibido);
  const totaltopay_order = JSON.parse(req.query.totaltopay_order);
  const id_order = JSON.parse(req.query.id_order);
  const preference = {
    items: [
      {
        //id: req.body.id_payment,
        title: "Pago de orden",
        description: "Pago total de pedido",
        quantity: 1,
        unit_price: totaltopay_order,
      },
    ],
    back_urls: {
      success:
        `https://${process.env.HOST}:80/api/mecardopago/mpago/redirect` +
        "?id_order=" +
        id_order,
      failure:
        `https://${process.env.HOST}:80/api/mecardopago/mpago/redirect` +
        "?id_order=" +
        id_order,
      pending:
        `https://${process.env.HOST}:80/api/mecardopago/mpago/redirect` +
        "?id_order=" +
        id_order,
      /* success: process.env.BACK_URL_SUCCESS,
      failure: process.env.BACK_URL_FAILURE, */
    },
    auto_return: "approved",
    //external_reference: req.body.id_payment,
  };

  mercadopago.preferences
    .create(preference)
    .then(function (response) {
      // En esta instancia deberĂĄs asignar el valor dentro de response.body.id por el ID de preferencia solicitado en el siguiente paso
      res.redirect(response.body.init_point);
    })
    .catch(function (error) {
      console.log(error);
    });
});

router.get("/mpago/redirect", (req, res) => {
  pedidos
    .updateEstado(JSON.parse(req.query.id_order), 2)
    .then((result) => {
      storage.removeItem("jwt");
      res
        .status(301)
        .redirect(
          `https://${process.env.HOST_FRONT}/#/orderGenerate?estado=Completado`
        );
    })
    .catch((err) => {
      res.redirect("/");
    });
});

//End Mercado Pago

module.exports = router;
