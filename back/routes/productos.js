const express = require("express");
const router = express.Router();
// sirve para recibir req
router.use(express.urlencoded({ extended: true }));
// traigo el array de datos de los productos
const productos = require("../controllers/productos.js");
const midUsers = require("../middlewares/usuarios");
/**
 * @swagger
 * tags:
 *   name: Productos
 *   description: CRUD de productos (Solo Administradores).
 * paths:
 *   /api/productos:
 *    get:
 *      tags: [Productos]
 *      description: lista todos los productos
 *      summary: "Lista de todos los productos"
 *      security:
 *        - jwt: []
 *      responses:
 *        200:
 *          description: Success
 */
router.get("/", function (req, res) {
  productos
    .get()
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data find Successfully",
        data: result,
      });
    })
    .catch((error) => {
      console.log(error);
      /*  res.status(400).send({
        message: "Unable to find data",
        errors: error,
        status: 400
      }); */
    });
});
/**
 * @swagger
 * paths:
 *   /api/productos:
 *    post:
 *      tags: [Productos]
 *      description: Crea un nuevo producto
 *      summary: "Creación de un nuevo producto"
 *      parameters:
 *       - name: descripcion
 *         description: Descripción del producto que desea dar de alta
 *         in: formData
 *         required: true
 *         type: string
 *       - name: precio
 *         description: Precio del producto
 *         in: formData
 *         required: true
 *         type: integer
 *      security:
 *        - jwt: []
 *      responses:
 *        200:
 *          description: Success
 *
 */
router.post("/", midUsers.userAdmin, function (req, res) {
  productos
    .add(req)
    .then(() => {
      res.status(200).send({
        status: 200,
        message: "Data Save Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to insert data",
        errors: error,
        status: 400,
      });
    });
});
/**
 * @swagger
 * paths:
 *   /api/productos/{id}:
 *    put:
 *      tags: [Productos]
 *      description: Actualizar un producto
 *      summary: "Actualizar un producto"
 *      parameters:
 *       - name: id
 *         description: Id del producto para actualizar
 *         in: path
 *         required: true
 *         type: integer
 *       - name: descripcion
 *         description: Descripción del producto que desea modificar (si no desea modificar dejar en blanco)
 *         in: formData
 *         required: false
 *         type: string
 *       - name: precio
 *         description: Precio del producto (si no desea modificar dejar en blanco)
 *         in: formData
 *         required: false
 *         type: integer
 *      security:
 *        - jwt: []
 *      responses:
 *        200:
 *          description: Success
 */
router.put("/:id", midUsers.userAdmin, function (req, res) {
  productos
    .update(req)
    .then((result) => {
      res.status(200).send({
        status: 200,
        message: "Data Update Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: `Unable to Update data. `,
        errors: `${error}`,
        status: 400,
      });
    });
});
/**
 * @swagger
 * paths:
 *   /api/productos/{id}:
 *    delete:
 *      tags: [Productos]
 *      description: Elimina un producto de acuerdo a su id
 *      summary: "Elimina un producto"
 *      parameters:
 *      - name: id
 *        description: Id del producto que desea eliminar
 *        in: path
 *        type: integer
 *        required: true
 *      security:
 *        - jwt: []
 *      responses:
 *        200:
 *          description: Success
 */
router.delete("/:id", midUsers.userAdmin, function (req, res) {
  productos
    .delet(req)
    .then(() => {
      res.status(200).send({
        status: 200,
        message: "Data Delete Successfully",
      });
    })
    .catch((error) => {
      res.status(400).send({
        message: "Unable to Delete data",
        errors: error,
        status: 400,
      });
    });
});

/**
 * @swagger
 * definitions:
 *   Productos:
 *     type: "object"
 *     properties:
 *       id:
 *         type: "integer"
 *         format: "int64"
 *       descripcion:
 *         type: "string"
 *       precio:
 *         type: "integer"
 *         format: "int64"
 */

module.exports = router;
